/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.spargonaut.i18n


import java.util.ArrayList
import java.util.regex.Pattern


/**
 * [ISO 639-2](http://en.wikipedia.org/wiki/ISO_639-2)
 * language code (3-letter lowercase code).
 *
 *
 *
 * Most languages have just one ISO 639-2 code, but there are some languages
 * that have 2 codes, ISO 639-2/T code ("terminological" code) and ISO 639-2/B
 * code ("bibliographic" code). The table below lists up langueses having two
 * ISO 639-2 codes.
 *
 *
 * <table border="1" style="border-collapse: collapse" cellpadding="5">
 * <tr bgcolor="orange">
 * <th>ISO 639-1</th>
 * <th>ISO 639-2/T</th>
 * <th>ISO 639-2/B</th>
 * <th>Language</th>
</tr> *
 * <tr>
 * <td>[bo][LanguageCode.bo]</td>
 * <td>[bod][LanguageAlpha3Code.bod]</td>
 * <td>[tib][LanguageAlpha3Code.tib]</td>
 * <td>[Tibetan](http://en.wikipedia.org/wiki/Standard_Tibetan)</td>
</tr> *
 * <tr>
 * <td>[eu][LanguageCode.eu]</td>
 * <td>[eus][LanguageAlpha3Code.eus]</td>
 * <td>[baq][LanguageAlpha3Code.baq]</td>
 * <td>[Basque](http://en.wikipedia.org/wiki/Basque_language)</td>
</tr> *
 * <tr>
 * <td>[cs][LanguageCode.cs]</td>
 * <td>[ces][LanguageAlpha3Code.ces]</td>
 * <td>[cze][LanguageAlpha3Code.cze]</td>
 * <td>[Czech](http://en.wikipedia.org/wiki/Czech_language)</td>
</tr> *
 * <tr>
 * <td>[cy][LanguageCode.cy]</td>
 * <td>[cym][LanguageAlpha3Code.cym]</td>
 * <td>[wel][LanguageAlpha3Code.wel]</td>
 * <td>[Welsh](http://en.wikipedia.org/wiki/Welsh_language)</td>
</tr> *
 * <tr>
 * <td>[de][LanguageCode.de]</td>
 * <td>[deu][LanguageAlpha3Code.deu]</td>
 * <td>[ger][LanguageAlpha3Code.ger]</td>
 * <td>[German](http://en.wikipedia.org/wiki/German_language)</td>
</tr> *
 * <tr>
 * <td>[el][LanguageCode.el]</td>
 * <td>[ell][LanguageAlpha3Code.ell]</td>
 * <td>[gre][LanguageAlpha3Code.gre]</td>
 * <td>[Greek](http://en.wikipedia.org/wiki/Greek_language)</td>
</tr> *
 * <tr>
 * <td>[fa][LanguageCode.fa]</td>
 * <td>[fas][LanguageAlpha3Code.fas]</td>
 * <td>[per][LanguageAlpha3Code.per]</td>
 * <td>[Persian](http://en.wikipedia.org/wiki/Persian_language)</td>
</tr> *
 * <tr>
 * <td>[fr][LanguageCode.fr]</td>
 * <td>[fra][LanguageAlpha3Code.fra]</td>
 * <td>[fre][LanguageAlpha3Code.fre]</td>
 * <td>[French](http://en.wikipedia.org/wiki/French_language)</td>
</tr> *
 * <tr>
 * <td>[hy][LanguageCode.hy]</td>
 * <td>[hye][LanguageAlpha3Code.hye]</td>
 * <td>[arm][LanguageAlpha3Code.arm]</td>
 * <td>[Armenian](http://en.wikipedia.org/wiki/Armenian_language)</td>
</tr> *
 * <tr>
 * <td>[is][LanguageCode. is]</td>
 * <td>[isl][LanguageAlpha3Code.isl]</td>
 * <td>[ice][LanguageAlpha3Code.ice]</td>
 * <td>[Icelandic](http://en.wikipedia.org/wiki/Icelandic_language)</td>
</tr> *
 * <tr>
 * <td>[ka][LanguageCode.ka]</td>
 * <td>[kat][LanguageAlpha3Code.kat]</td>
 * <td>[geo][LanguageAlpha3Code.geo]</td>
 * <td>[Georgian](http://en.wikipedia.org/wiki/Georgian_language)</td>
</tr> *
 * <tr>
 * <td>[mi][LanguageCode.mi]</td>
 * <td>[mri][LanguageAlpha3Code.mri]</td>
 * <td>[mao][LanguageAlpha3Code.mao]</td>
 * <td>[M&#257;ori](http://en.wikipedia.org/wiki/M%C4%81ori_language)
</td> *
</tr> *
 * <tr>
 * <td>[mk][LanguageCode.mk]</td>
 * <td>[mkd][LanguageAlpha3Code.mkd]</td>
 * <td>[mac][LanguageAlpha3Code.mac]</td>
 * <td>[Macedonian](http://en.wikipedia.org/wiki/Macedonian_language)
</td> *
</tr> *
 * <tr>
 * <td>[ms][LanguageCode.ms]</td>
 * <td>[msa][LanguageAlpha3Code.msa]</td>
 * <td>[may][LanguageAlpha3Code.may]</td>
 * <td>[Malay](http://en.wikipedia.org/wiki/Malay_language)</td>
</tr> *
 * <tr>
 * <td>[my][LanguageCode.my]</td>
 * <td>[mya][LanguageAlpha3Code.mya]</td>
 * <td>[bur][LanguageAlpha3Code.bur]</td>
 * <td>[Burmese](http://en.wikipedia.org/wiki/Burmese_language)</td>
</tr> *
 * <tr>
 * <td>[nl][LanguageCode.nl]</td>
 * <td>[nld][LanguageAlpha3Code.nld]</td>
 * <td>[dut][LanguageAlpha3Code.dut]</td>
 * <td>[Dutch](http://en.wikipedia.org/wiki/Dutch_language)</td>
</tr> *
 * <tr>
 * <td>[ro][LanguageCode.ro]</td>
 * <td>[ron][LanguageAlpha3Code.ron]</td>
 * <td>[rum][LanguageAlpha3Code.rum]</td>
 * <td>[Romanian](http://en.wikipedia.org/wiki/Romanian_language)</td>
</tr> *
 * <tr>
 * <td>[sk][LanguageCode.sk]</td>
 * <td>[slk][LanguageAlpha3Code.slk]</td>
 * <td>[slo][LanguageAlpha3Code.slo]</td>
 * <td>[Slovak](http://en.wikipedia.org/wiki/Slovak_language)</td>
</tr> *
 * <tr>
 * <td>[sq][LanguageCode.sq]</td>
 * <td>[sqi][LanguageAlpha3Code.sqi]</td>
 * <td>[alb][LanguageAlpha3Code.alb]</td>
 * <td>[Albanian](http://en.wikipedia.org/wiki/Albanian_language)</td>
</tr> *
</table> *
 *
 *
 *
 * ISO 639-2 code for [Newari](http://en.wikipedia.org/wiki/Newari_language) is `new`, but in this enum, the corresponding entry
 * is not `new` but [.New] (the first letter is capital),
 * because `new` is a special word for Java programming language.
 *
 *
 *
 */
enum class LanguageAlpha3Code private constructor(
        /**
         * Language name.
         */
        /**
         * Get the language name.
         *
         * @return
         * The language name.
         */
        val languageName: String) {
    /**
     * Undefined.
     *
     *
     *
     * This is not an official ISO 639-2 code.
     *
     *
     *
     * @see .und und: Undetermined
     *
     * @see .zxx zxx: No linguistic content
     */
    undefined("Undefined") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.undefined
    },

    /**
     * [Afar](http://en.wikipedia.org/wiki/Afar_language)
     * ([aa][LanguageCode.aa]).
     */
    aar("Afar") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.aa
    },

    /**
     * [Austro-Asiatic languages](http://en.wikipedia.org/wiki/Austro-Asiatic_languages)
     *
     *
     */
    aav("Austro-Asiatic languages"),

    /**
     * [Abkhaz](http://en.wikipedia.org/wiki/Abkhaz_language)
     * ([ab][LanguageCode.ab]).
     */
    abk("Abkhaz") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ab
    },

    /**
     * [Achinese](http://en.wikipedia.org/wiki/Achinese_language)
     *
     *
     */
    ace("Achinese"),

    /**
     * [Acoli](http://en.wikipedia.org/wiki/Acoli_language)
     *
     *
     */
    ach("Acoli"),

    /**
     * [Adangme](http://en.wikipedia.org/wiki/Adangme_language)
     *
     *
     */
    ada("Adangme"),

    /**
     * [Adyghe](http://en.wikipedia.org/wiki/Adyghe_language)
     *
     *
     */
    ady("Adyghe"),

    /**
     * [Afro-Asiatic languages](http://en.wikipedia.org/wiki/Afro-Asiatic_languages)
     *
     *
     */
    afa("Afro-Asiatic languages"),

    /**
     * [Afrihili](http://en.wikipedia.org/wiki/Afrihili_language)
     *
     *
     */
    afh("Afrihili"),

    /**
     * [Afrikaans](http://en.wikipedia.org/wiki/Afrikaans_language)
     * ([af][LanguageCode.af]).
     */
    afr("Afrikaans") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.af
    },

    /**
     * [Ainu (Japan)](http://en.wikipedia.org/wiki/Ainu_language_(Japan))
     *
     *
     */
    ain("Ainu (Japan)"),

    /**
     * [Akan](http://en.wikipedia.org/wiki/Akan_language)
     * ([ak][LanguageCode.ak]).
     */
    aka("Akan") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ak
    },

    /**
     * [Akkadian](http://en.wikipedia.org/wiki/Akkadian_language)
     *
     *
     */
    akk("Akkadian"),

    /**
     * [Albanian](http://en.wikipedia.org/wiki/Albanian_language)
     * ([sq][LanguageCode.sq]) for bibliographic applications.
     *
     * @see .sqi
     */
    alb("Albanian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.sq


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = sqi
    },

    /**
     * [Aleut](http://en.wikipedia.org/wiki/Aleut_language)
     *
     *
     */
    ale("Aleut"),

    /**
     * [Algonquian languages](http://en.wikipedia.org/wiki/Algonquian_languages)
     *
     *
     */
    alg("Algonquian languages"),

    /**
     * [Southern Altai](http://en.wikipedia.org/wiki/Southern_Altai_language)
     *
     *
     */
    alt("Southern Altai"),

    /**
     * [Atlantic-Congo languages<a></a>
 *
 *
](http://en.wikipedia.org/wiki/Atlantic-Congo_languages) */
    alv("Atlantic-Congo languages"),

    /**
     * [Amharic](http://en.wikipedia.org/wiki/Amharic_language)
     * ([am][LanguageCode.am]).
     */
    amh("Amharic") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.am
    },

    /**
     * [Old English](http://en.wikipedia.org/wiki/Old_English_language) (ca&#0046; 450-1100)
     *
     *
     */
    ang("Old English"),

    /**
     * [Angika](http://en.wikipedia.org/wiki/Angika_language)
     *
     *
     */
    anp("Angika"),

    /**
     * [Apache languages](http://en.wikipedia.org/wiki/Apache_languages)
     *
     *
     */
    apa("Apache languages"),

    /**
     * [Alacalufan languages](http://en.wikipedia.org/wiki/Alacalufan_languages)
     *
     *
     */
    aqa("Alacalufan languages"),

    /**
     * [Algic languages](http://en.wikipedia.org/wiki/Algic_languages)
     *
     *
     */
    aql("Algic languages"),

    /**
     * [Arabic](http://en.wikipedia.org/wiki/Arabic_language)
     * ([ar][LanguageCode.ar]).
     */
    ara("Arabic") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ar
    },

    /**
     * [Official Aramaic](http://en.wikipedia.org/wiki/Official_Aramaic_language) (700-300 BCE)
     *
     *
     */
    arc("Official Aramaic"),

    /**
     * [Aragonese](http://en.wikipedia.org/wiki/Aragonese_language)
     * ([an][LanguageCode.an]).
     */
    arg("Aragonese") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.an
    },

    /**
     * [Armenian](http://en.wikipedia.org/wiki/Armenian_language)
     * ([hy][LanguageCode.hy]) for bibliographic applications.
     *
     * @see .hye
     */
    arm("Armenian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.hy


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = hye
    },

    /**
     * [Mapudungun](http://en.wikipedia.org/wiki/Mapudungun_language)
     *
     *
     */
    arn("Mapudungun"),

    /**
     * [Arapaho](http://en.wikipedia.org/wiki/Arapaho_language)
     *
     *
     */
    arp("Arapaho"),

    /**
     * [Artificial languages](http://en.wikipedia.org/wiki/Artificial_languages)
     *
     *
     */
    art("Artificial languages"),

    /**
     * [Arawak](http://en.wikipedia.org/wiki/Arawak_language)
     *
     *
     */
    arw("Arawak"),

    /**
     * [Assamese](http://en.wikipedia.org/wiki/Assamese_language)
     * ([as][LanguageCode. as]).
     */
    asm("Assamese") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.`as`
    },

    /**
     * [Asturian](http://en.wikipedia.org/wiki/Asturian_language)
     *
     *
     */
    ast("Asturian"),

    /**
     * [Athapascan languages](http://en.wikipedia.org/wiki/Athapascan_languages)
     *
     *
     */
    ath("Athapascan languages"),

    /**
     * [Arauan languages](http://en.wikipedia.org/wiki/Arauan_languages)
     *
     *
     */
    auf("Arauan languages"),

    /**
     * [Australian languages](http://en.wikipedia.org/wiki/Australian_languages)
     *
     *
     */
    aus("Australian languages"),

    /**
     * [Avaric](http://en.wikipedia.org/wiki/Avar_language)
     * ([av][LanguageCode.av]).
     */
    ava("Avaric") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.av
    },

    /**
     * [Avestan](http://en.wikipedia.org/wiki/Avestan_language)
     * ([ae][LanguageCode.ae]).
     */
    ave("Avestan") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ae
    },

    /**
     * [Awadhi](http://en.wikipedia.org/wiki/Awadhi_language)
     *
     *
     */
    awa("Awadhi"),

    /**
     * [Arawakan languages](http://en.wikipedia.org/wiki/Arawakan_languages)
     *
     *
     */
    awd("Arawakan languages"),

    /**
     * [Aymara](http://en.wikipedia.org/wiki/Aymara_language)
     * ([ay][LanguageCode.ay]).
     */
    aym("Aymara") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ay
    },

    /**
     * [Uto-Aztecan languages](http://en.wikipedia.org/wiki/Uto-Aztecan_languages)
     *
     *
     */
    azc("Uto-Aztecan languages"),

    /**
     * [Azerbaijani](http://en.wikipedia.org/wiki/Azerbaijani_language)
     * ([az][LanguageCode.az]).
     */
    aze("Azerbaijani") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.az
    },

    /**
     * [Banda languages](http://en.wikipedia.org/wiki/Banda_languages)
     *
     *
     */
    bad("Banda languages"),

    /**
     * [Bamileke languages](http://en.wikipedia.org/wiki/Bamileke_languages)
     *
     *
     */
    bai("Bamileke languages"),

    /**
     * [Bashkir](http://en.wikipedia.org/wiki/Bashkir_language)
     * ([ba][LanguageCode.ba]).
     */
    bak("Bashkir") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ba
    },

    /**
     * [Baluchi](http://en.wikipedia.org/wiki/Baluchi_language)
     */
    bal("Baluchi"),

    /**
     * [Bambara](http://en.wikipedia.org/wiki/Bambara_language)
     * ([bm][LanguageCode.bm]).
     */
    bam("Bambara") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.bm
    },

    /**
     * [Balinese](http://en.wikipedia.org/wiki/Balinese_language)
     *
     *
     */
    ban("Balinese"),

    /**
     * [Basque](http://en.wikipedia.org/wiki/Basque_language)
     * ([eu][LanguageCode.eu]) for bibliographic applications.
     *
     * @see .eus
     */
    baq("Basque") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.eu


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = eus
    },

    /**
     * [Basa (Cameroon)](http://en.wikipedia.org/wiki/Basa_language_(Cameroon))
     *
     *
     */
    bas("Basa (Cameroon)"),

    /**
     * [Baltic languages](http://en.wikipedia.org/wiki/Baltic_languages)
     *
     *
     */
    bat("Baltic languages"),

    /**
     * [Beja](http://en.wikipedia.org/wiki/Beja_language)
     *
     *
     */
    bej("Beja"),

    /**
     * [Belarusian](http://en.wikipedia.org/wiki/Belarusian_language)
     * ([be][LanguageCode.be]).
     */
    bel("Belarusian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.be
    },

    /**
     * [Bemba (Zambia)](http://en.wikipedia.org/wiki/Bemba_language_(Zambia))
     *
     *
     */
    bem("Bemba (Zambia)"),

    /**
     * [Bengali](http://en.wikipedia.org/wiki/Bengali_language)
     * ([bn][LanguageCode.bn]).
     */
    ben("Bengali") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.bn
    },

    /**
     * [Berber languages](http://en.wikipedia.org/wiki/Berber_languages)
     *
     *
     */
    ber("Berber languages"),

    /**
     * [Bhojpuri](http://en.wikipedia.org/wiki/Bhojpuri_language)
     *
     *
     */
    bho("Bhojpuri"),

    /**
     * [Bihari](http://en.wikipedia.org/wiki/Bihari_languages)
     * ([bh][LanguageCode.bh]).
     */
    bih("Bihari languages") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.bh
    },

    /**
     * [Bikol](http://en.wikipedia.org/wiki/Bikol_language)
     *
     *
     */
    bik("Bikol"),

    /**
     * [Bini](http://en.wikipedia.org/wiki/Bini_language)
     *
     *
     */
    bin("Bini"),

    /**
     * [Bislama](http://en.wikipedia.org/wiki/Bislama_language)
     * ([bi][LanguageCode.bi]).
     */
    bis("Bislama") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.bi
    },

    /**
     * [Siksika](http://en.wikipedia.org/wiki/Siksika_language)
     *
     *
     */
    bla("Siksika"),

    /**
     * [Bantu languages](http://en.wikipedia.org/wiki/Bantu_languages)
     *
     *
     */
    bnt("Bantu languages"),

    /**
     * [Tibetan](http://en.wikipedia.org/wiki/Standard_Tibetan)
     * ([bo][LanguageCode.bo]) for terminology applications.
     *
     * @see .tib
     */
    bod("Tibetan") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.bo


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = tib
    },

    /**
     * [Bosnian](http://en.wikipedia.org/wiki/Bosnian_language)
     * ([bs][LanguageCode.bs]).
     */
    bos("Bosnian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.bs
    },

    /**
     * [Braj](http://en.wikipedia.org/wiki/Braj_language)
     *
     *
     */
    bra("Braj"),

    /**
     * [Breton](http://en.wikipedia.org/wiki/Breton_language)
     * ([br][LanguageCode.br]).
     */
    bre("Breton") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.br
    },

    /**
     * [Batak languages](http://en.wikipedia.org/wiki/Batak_languages)
     *
     *
     */
    btk("Batak languages"),

    /**
     * [Buriat](http://en.wikipedia.org/wiki/Buriat_language)
     *
     *
     */
    bua("Buriat"),

    /**
     * [Buginese](http://en.wikipedia.org/wiki/Buginese_language)
     *
     *
     */
    bug("Buginese"),

    /**
     * [Bulgarian](http://en.wikipedia.org/wiki/Bulgarian_language)
     * ([bg][LanguageCode.bg]).
     */
    bul("Bulgarian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.bg
    },

    /**
     * [Burmese](http://en.wikipedia.org/wiki/Burmese_language)
     * ([my][LanguageCode.my]) for bibliographic applications.
     *
     * @see .mya
     */
    bur("Burmese") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.my


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = mya
    },

    /**
     * [Bilin](http://en.wikipedia.org/wiki/Bilin_language)
     *
     *
     */
    byn("Bilin"),

    /**
     * [Caddo](http://en.wikipedia.org/wiki/Caddo_language)
     *
     *
     */
    cad("Caddo"),

    /**
     * [Central American Indian languages](http://en.wikipedia.org/wiki/Central_American_Indian_languages)
     *
     *
     */
    cai("Central American Indian languages"),

    /**
     * [Galibi Carib](http://en.wikipedia.org/wiki/Galibi_Carib_language)
     *
     *
     */
    car("Galibi Carib"),

    /**
     * [Catalan](http://en.wikipedia.org/wiki/Catalan_language)
     * ([ca][LanguageCode.ca]).
     */
    cat("Catalan") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ca
    },

    /**
     * [Caucasian languages](http://en.wikipedia.org/wiki/Caucasian_languages)
     *
     *
     */
    cau("Caucasian languages"),

    /**
     * [Chibchan languages](http://en.wikipedia.org/wiki/Chibchan_languages)
     *
     *
     */
    cba("Chibchan languages"),

    /**
     * [North Caucasian languages](http://en.wikipedia.org/wiki/North_Caucasian_languages)
     *
     *
     */
    ccn("North Caucasian languages"),

    /**
     * [South Caucasian languages](http://en.wikipedia.org/wiki/South_Caucasian_languages)
     *
     *
     */
    ccs("South Caucasian languages"),

    /**
     * [Chadic languages](http://en.wikipedia.org/wiki/Chadic_languages)
     *
     *
     */
    cdc("Chadic languages"),

    /**
     * [Caddoan languages](http://en.wikipedia.org/wiki/Caddoan_languages)
     *
     *
     */
    cdd("Caddoan languages"),

    /**
     * [Cebuano](http://en.wikipedia.org/wiki/Cebuano_language)
     *
     *
     */
    ceb("Cebuano"),

    /**
     * [Celtic languages](http://en.wikipedia.org/wiki/Celtic_languages)
     *
     *
     */
    cel("Celtic languages"),

    /**
     * [Czech](http://en.wikipedia.org/wiki/Czech_language)
     * ([cs][LanguageCode.cs]) for terminology applications.
     *
     * @see .cze
     */
    ces("Czech") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.cs


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = cze
    },

    /**
     * [Chamorro](http://en.wikipedia.org/wiki/Chamorro_language)
     * ([ch][LanguageCode.ch]).
     */
    cha("Chamorro") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ch
    },

    /**
     * [Chibcha](http://en.wikipedia.org/wiki/Chibcha_language)
     *
     *
     */
    chb("Chibcha"),

    /**
     * [Chechen](http://en.wikipedia.org/wiki/Chechen_language)
     * ([ce][LanguageCode.ce]).
     */
    che("Chechen") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ce
    },

    /**
     * [Chagatai](http://en.wikipedia.org/wiki/Chagatai_language)
     *
     *
     */
    chg("Chagatai"),

    /**
     * [Chinese](http://en.wikipedia.org/wiki/Chinese_language)
     * ([zh][LanguageCode.zh]) for bibliographic applications.
     *
     * @see .zho
     */
    chi("Chinese") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.zh


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = zho
    },

    /**
     * [Chuukese](http://en.wikipedia.org/wiki/Chuukese_language)
     *
     *
     */
    chk("Chuukese"),

    /**
     * [Mari (Russia)](http://en.wikipedia.org/wiki/Mari_language_(Russia))
     *
     *
     */
    chm("Mari (Russia)"),

    /**
     * [Chinook jargon](http://en.wikipedia.org/wiki/Chinook_jargon_language)
     *
     *
     */
    chn("Chinook jargon"),

    /**
     * [Choctaw](http://en.wikipedia.org/wiki/Choctaw_language)
     *
     *
     */
    cho("Choctaw"),

    /**
     * [Chipewyan](http://en.wikipedia.org/wiki/Chipewyan_language)
     *
     *
     */
    chp("Chipewyan"),

    /**
     * [Cherokee](http://en.wikipedia.org/wiki/Cherokee_language)
     *
     *
     */
    chr("Cherokee"),

    /**
     * [Church
 * Slavonic](http://en.wikipedia.org/wiki/Old_Church_Slavonic)
     * ([cu][LanguageCode.cu]).
     */
    chu("Church Slavic") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.cu
    },

    /**
     * [Chuvash](http://en.wikipedia.org/wiki/Chuvash_language)
     * ([cv][LanguageCode.cv]).
     */
    chv("Chuvash") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.cv
    },

    /**
     * [Cheyenne](http://en.wikipedia.org/wiki/Cheyenne_language)
     *
     *
     */
    chy("Cheyenne"),

    /**
     * [Chamic languages](http://en.wikipedia.org/wiki/Chamic_languages)
     *
     *
     */
    cmc("Chamic languages"),

    /**
     * [Coptic](http://en.wikipedia.org/wiki/Coptic_language)
     *
     *
     */
    cop("Coptic"),

    /**
     * [Cornish](http://en.wikipedia.org/wiki/Cornish_language)
     * ([kw][LanguageCode.kw]).
     */
    cor("Comish") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.kw
    },

    /**
     * [Corsican](http://en.wikipedia.org/wiki/Corsican_language)
     * ([co][LanguageCode.co]).
     */
    cos("Corsican") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.co
    },

    /**
     * [English based Creoles](http://en.wikipedia.org/wiki/English_based_Creoles) and
     * [pidgins](http://en.wikipedia.org/wiki/English_based_pidgins)
     *
     *
     */
    cpe("English based Creoles and pidgins"),

    /**
     * [French-Based Creoles](http://en.wikipedia.org/wiki/French-Based_Creoles) and
     * [pidgins](http://en.wikipedia.org/wiki/French-Based_pidgins)
     *
     *
     */
    cpf("French-Based Creoles and pidgins"),

    /**
     * [Portuguese-Based Creoles](http://en.wikipedia.org/wiki/Portuguese-Based_Creoles) and
     * [pidgins](http://en.wikipedia.org/wiki/Portuguese-Based_pidgins)
     *
     *
     */
    cpp("Portuguese-Based Creoles and pidgins"),

    /**
     * [Cree](http://en.wikipedia.org/wiki/Cree_language)
     * ([cr][LanguageCode.cr]).
     */
    cre("Cree") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.cr
    },

    /**
     * [Crimean Tatar](http://en.wikipedia.org/wiki/Crimean_Tatar_language)
     *
     *
     */
    crh("Crimean Tatar"),

    /**
     * [Creoles](http://en.wikipedia.org/wiki/Creole_language) and
     * [pidgins](http://en.wikipedia.org/wiki/Pidgin_language)
     *
     *
     */
    crp("Creoles and pidgins"),

    /**
     * [Kashubian](http://en.wikipedia.org/wiki/Kashubian_language)
     *
     *
     */
    csb("Kashubian"),

    /**
     * [Central Sudanic languages](http://en.wikipedia.org/wiki/Central_Sudanic_languages)
     *
     *
     */
    csu("Central Sudanic languages"),

    /**
     * [Cushitic languages](http://en.wikipedia.org/wiki/Cushitic_languages)
     *
     *
     */
    cus("Cushitic languages"),

    /**
     * [Welsh](http://en.wikipedia.org/wiki/Welsh_language)
     * ([cy][LanguageCode.cy]) for terminology applications.
     *
     * @see .wel
     */
    cym("Welsh") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.cy


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = wel
    },

    /**
     * [Czech](http://en.wikipedia.org/wiki/Czech_language)
     * ([cs][LanguageCode.cs]) for bibliographic applications.
     *
     * @see .ces
     */
    cze("Czech") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.cs


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = ces
    },

    /**
     * [Dakota](http://en.wikipedia.org/wiki/Dakota_language)
     *
     *
     */
    dak("Dakota"),

    /**
     * [Danish](http://en.wikipedia.org/wiki/Danish_language)
     * [da][LanguageCode.da]).
     */
    dan("Danish") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.da
    },

    /**
     * [Dargwa](http://en.wikipedia.org/wiki/Dargwa_language)
     *
     *
     */
    dar("Dargwa"),

    /**
     * [Land Dayak languages](http://en.wikipedia.org/wiki/Land_Dayak_languages)
     *
     *
     */
    day("Land Dayak languages"),

    /**
     * [Delaware](http://en.wikipedia.org/wiki/Delaware_language)
     *
     *
     */
    del("Delaware"),

    /**
     * [Slave (Athapascan)](http://en.wikipedia.org/wiki/Slave_language_(Athapascan))
     *
     *
     */
    den("Slave (Athapascan)"),

    /**
     * [German](http://en.wikipedia.org/wiki/German_language)
     * ([de][LanguageCode.de]) for terminology applications.
     *
     * @see .ger
     */
    deu("German") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.de


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = ger
    },

    /**
     * [Dogrib](http://en.wikipedia.org/wiki/Dogrib_language)
     *
     *
     */
    dgr("Dogrib"),

    /**
     * [Dinka](http://en.wikipedia.org/wiki/Dinka_language)
     *
     *
     */
    din("Dinka"),

    /**
     * [Dhivehi](http://en.wikipedia.org/wiki/Dhivehi_language)
     * ([dv][LanguageCode.dv]).
     */
    div("Dhivehi") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.dv
    },

    /**
     * [Mande languages](http://en.wikipedia.org/wiki/Mande_languages)
     *
     *
     */
    dmn("Mande languages"),

    /**
     * [Dogri](http://en.wikipedia.org/wiki/Dogri_language) (macrolanguage)
     *
     *
     */
    doi("Dogri"),

    /**
     * [Dravidian languages](http://en.wikipedia.org/wiki/Dravidian_languages)
     *
     *
     */
    dra("Dravidian languages"),

    /**
     * [Lower Sorbian](http://en.wikipedia.org/wiki/Lower_Sorbian_language)
     *
     *
     */
    dsb("Lower Sorbian"),

    /**
     * [Duala](http://en.wikipedia.org/wiki/Duala_language)
     *
     *
     */
    dua("Duala"),

    /**
     * [Middle Dutch](http://en.wikipedia.org/wiki/Middle_Dutch_language) (ca. 1050-1350)
     *
     *
     */
    dum("Middle Dutch"),

    /**
     * [Dutch](http://en.wikipedia.org/wiki/Dutch_language)
     * ([nl][LanguageCode.nl]) for bibliography applications.
     *
     * @see .nld
     */
    dut("Dutch") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.nl


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = nld
    },

    /**
     * [Dyula](http://en.wikipedia.org/wiki/Dyula_language)
     *
     *
     */
    dyu("Dyula"),

    /**
     * [Dzongkha](http://en.wikipedia.org/wiki/Dzongkha_language)
     * ([dz][LanguageCode.dz]).
     */
    dzo("Dzongkha") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.dz
    },

    /**
     * [Efik](http://en.wikipedia.org/wiki/Efik_language)
     *
     *
     */
    efi("Efik"),

    /**
     * [Egyptian languages](http://en.wikipedia.org/wiki/Egyptian_languages)
     *
     *
     */
    egx("Egyptian languages"),

    /**
     * [Egyptian (Ancient)](http://en.wikipedia.org/wiki/Egyptian_language_(Ancient))
     *
     *
     */
    egy("Egyptian (Ancient)"),

    /**
     * [Ekajuk](http://en.wikipedia.org/wiki/Ekajuk_language)
     *
     *
     */
    eka("Ekajuk"),

    /**
     * [Modern Greek](http://en.wikipedia.org/wiki/Modern_Greek_language) (1453-)
     * ([el][LanguageCode.el]) for terminology applications.
     *
     * @see .gre Modern Greek
     * @see .grc Acient Greek
     */
    ell("Modern Greek") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.el


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = gre
    },

    /**
     * [Elamite](http://en.wikipedia.org/wiki/Elamite_language)
     *
     *
     */
    elx("Elamite"),

    /**
     * [English](http://en.wikipedia.org/wiki/English_language)
     * ([en][LanguageCode.en]).
     */
    eng("English") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.en
    },

    /**
     * [Middle English](http://en.wikipedia.org/wiki/Middle_English_language) (1100-1500)
     */
    enm("Middle English"),

    /**
     * [Esperanto](http://en.wikipedia.org/wiki/Esperanto)
     * ([eo][LanguageCode.eo]).
     */
    epo("Esperanto") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.eo
    },

    /**
     * [Estonian](http://en.wikipedia.org/wiki/Estonian_language)
     * ([et][LanguageCode.et]).
     */
    est("Estonian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.et
    },

    /**
     * [Eskimo-Aleut languages](http://en.wikipedia.org/wiki/Eskimo-Aleut_languages)
     *
     *
     */
    esx("Eskimo-Aleut languages"),

    /**
     * [Basque](http://en.wikipedia.org/wiki/Basque_language) (family)
     *
     *
     */
    euq("Basque"),

    /**
     * [Basque](http://en.wikipedia.org/wiki/Basque_language) (family)
     * ([eu][LanguageCode.eu]) for terminology applications.
     *
     * @see .baq
     */
    eus("Basque (family)") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.eu


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = baq
    },

    /**
     * [Ewe](http://en.wikipedia.org/wiki/Ewe_language)
     * ([ee][LanguageCode.ee]).
     */
    ewe("Ewe") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ee
    },

    /**
     * [Ewondo](http://en.wikipedia.org/wiki/Ewondo_language)
     *
     *
     */
    ewo("Ewondo"),

    /**
     * [Fang (Equatorial Guinea)](http://en.wikipedia.org/wiki/Fang_language_(Equatorial_Guinea))
     *
     *
     */
    fan("Fang (Equatorial Guinea)"),

    /**
     * [Faroese](http://en.wikipedia.org/wiki/Faroese_language)
     * ([fo][LanguageCode.fo]).
     */
    fao("Faroese") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.fo
    },

    /**
     * [Persian](http://en.wikipedia.org/wiki/Persian_language)
     * ([fa][LanguageCode.fa]) for terminology applications.
     *
     * @see .per
     */
    fas("Persian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.fa


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = per
    },

    /**
     * [Fanti](http://en.wikipedia.org/wiki/Fanti_language)
     *
     *
     */
    fat("Fanti"),

    /**
     * [Fijian](http://en.wikipedia.org/wiki/Fijian_language)
     * ([fj][LanguageCode.fj]).
     */
    fij("Fijian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.fj
    },

    /**
     * [Filipino](http://en.wikipedia.org/wiki/Filipino_language)
     *
     *
     */
    fil("Filipino"),

    /**
     * [Finnish](http://en.wikipedia.org/wiki/Finnish_language)
     * ([fi][LanguageCode.fi]).
     */
    fin("Finnish") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.fi
    },

    /**
     * [Finno-Ugrian languages](http://en.wikipedia.org/wiki/Finno-Ugrian_languages)
     *
     *
     */
    fiu("Finno-Ugrian languages"),

    /**
     * [Fon](http://en.wikipedia.org/wiki/Fon_language)
     *
     *
     */
    fon("Fon"),

    /**
     * [Formosan languages](http://en.wikipedia.org/wiki/Formosan_languages)
     *
     *
     */
    fox("Formosan languages"),

    /**
     * [French](http://en.wikipedia.org/wiki/French_language)
     * ([fr][LanguageCode.fr]) for terminology applications.
     *
     * @see .fre
     */
    fra("French") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.fr


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = fre
    },

    /**
     * [French](http://en.wikipedia.org/wiki/French_language)
     * ([fr][LanguageCode.fr]) for bibliographic applications.
     *
     * @see .fra
     */
    fre("French") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.fr


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = fra
    },

    /**
     * [Middle French](http://en.wikipedia.org/wiki/Middle_French_language) (ca&#0046; 1400-1600)
     *
     *
     */
    frm("Middle French"),

    /**
     * [Old French](http://en.wikipedia.org/wiki/Old_French_language) (842-ca&#0046; 1400)
     *
     *
     */
    fro("Old French"),

    /**
     * [Northern Frisian](http://en.wikipedia.org/wiki/Northern_Frisian_language)
     *
     *
     */
    frr("Northern Frisian"),

    /**
     * [Eastern Frisian](http://en.wikipedia.org/wiki/Eastern_Frisian_language)
     *
     *
     */
    frs("Eastern Frisian"),

    /**
     * [West
 * Frisian](http://en.wikipedia.org/wiki/West_Frisian_language)
     * ([fy][LanguageCode.fy]).
     */
    fry("West Frisian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.fy
    },

    /**
     * [Fula](http://en.wikipedia.org/wiki/Fula_language)
     * ([ff][LanguageCode.ff]).
     */
    ful("Fula") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ff
    },

    /**
     * [Friulian](http://en.wikipedia.org/wiki/Friulian_language)
     *
     *
     */
    fur("Friulian"),

    /**
     * [Ga](http://en.wikipedia.org/wiki/Ga_language)
     *
     *
     */
    gaa("Ga"),

    /**
     * [Gayo](http://en.wikipedia.org/wiki/Gayo_language)
     *
     *
     */
    gay("Gayo"),

    /**
     * [Gbaya (Central African Republic)](http://en.wikipedia.org/wiki/Gbaya_language_(Central_African_Republic))
     *
     *
     */
    gba("Gbaya (Central African Republic)"),

    /**
     * [Germanic languages](http://en.wikipedia.org/wiki/Germanic_languages)
     *
     *
     */
    gem("Germanic languages"),

    /**
     * [Georgian](http://en.wikipedia.org/wiki/Georgian_language)
     * ([ka][LanguageCode.ka]) for bibliographic applications.
     *
     * @see .kat
     */
    geo("Georgian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ka


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = kat
    },

    /**
     * [German](http://en.wikipedia.org/wiki/German_language)
     * ([de][LanguageCode.de]) for bibliographic applications.
     *
     * @see .deu
     */
    ger("German") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.de


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = deu
    },

    /**
     * [Geez](http://en.wikipedia.org/wiki/Geez_language)
     *
     *
     */
    gez("Geez"),

    /**
     * [Gilbertese](http://en.wikipedia.org/wiki/Gilbertese_language)
     *
     *
     */
    gil("Gilbertese"),

    /**
     * [Scottish
 * Gaelic](http://en.wikipedia.org/wiki/Scottish_Gaelic_language)
     * ([gd][LanguageCode.gd]).
     */
    gla("Scottish Gaelic") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.gd
    },

    /**
     * [Irish](http://en.wikipedia.org/wiki/Irish_language)
     * ([ga][LanguageCode.ga]).
     */
    gle("Irish") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ga
    },

    /**
     * [Galician](http://en.wikipedia.org/wiki/Galician_language)
     * ([gl][LanguageCode.gl]).
     */
    glg("Galician") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.gl
    },

    /**
     * [Manx](http://en.wikipedia.org/wiki/Manx_language)
     * ([gv][LanguageCode.gv]).
     */
    glv("Manx") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.gv
    },

    /**
     * [East Germanic languages](http://en.wikipedia.org/wiki/East_Germanic_languages)
     *
     *
     */
    gme("East Germanic languages"),

    /**
     * [Middle High German](http://en.wikipedia.org/wiki/Middle_High_German_language) (ca&#0046; 1050-1500)
     *
     *
     */
    gmh("Middle High German"),

    /**
     * [North Germanic languages](http://en.wikipedia.org/wiki/North_Germanic_languages)
     *
     *
     */
    gmq("North Germanic languages"),

    /**
     * [West Germanic languages](http://en.wikipedia.org/wiki/West_Germanic_languages)
     *
     *
     */
    gmw("West Germanic languages"),

    /**
     * [Old High German](http://en.wikipedia.org/wiki/Old_High_German_language) (ca&#0046; 750-1050)
     *
     *
     */
    goh("Old High German"),

    /**
     * [Gondi](http://en.wikipedia.org/wiki/Gondi_language)
     *
     *
     */
    gon("Gondi"),

    /**
     * [Gorontalo](http://en.wikipedia.org/wiki/Gorontalo_language)
     *
     *
     */
    gor("Gorontalo"),

    /**
     * [Gothic](http://en.wikipedia.org/wiki/Gothic_language)
     *
     *
     */
    got("Gothic"),

    /**
     * [Grebo](http://en.wikipedia.org/wiki/Grebo_language)
     *
     *
     */
    grb("Grebo"),

    /**
     * [Ancient Greek](http://en.wikipedia.org/wiki/Ancient_Greek_language) (to 1453)
     *
     * @see .ell Modern Greek
     *
     */
    grc("Ancient Greek"),

    /**
     * [Modern Greek](http://en.wikipedia.org/wiki/Modern_Greek_language) (1453-)
     * ([el][LanguageCode.el]) for bibliographic applications.
     *
     * @see .ell Modern Greek
     * @see .grc Acient Greek
     */
    gre("Modern Greek") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.el


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = ell
    },

    /**
     * [Greek languages](http://en.wikipedia.org/wiki/Greek_languages)
     *
     *
     */
    grk("Greek languages"),

    /**
     * [Guaran&iacute
 * ;](http://en.wikipedia.org/wiki/Guaran%C3%AD_language)
     * ([gn][LanguageCode.gn]).
     */
    grn("Guaran\u00ED") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.gn
    },

    /**
     * [Swiss German](http://en.wikipedia.org/wiki/Swiss_German_language)
     *
     *
     */
    gsw("Swiss German"),

    /**
     * [Gujarati](http://en.wikipedia.org/wiki/Gujarati_language)
     * ([gu][LanguageCode.gu]).
     */
    guj("Gujarati") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.gu
    },

    /**
     * [Gwich&#x2bc;in](http://en.wikipedia.org/wiki/Gwich%CA%BCin_language)
     *
     *
     */
    gwi("Gwich\u02BCin"),

    /**
     * [Haida](http://en.wikipedia.org/wiki/Haida_language)
     *
     *
     */
    hai("Haida"),

    /**
     * [Haitian](http://en.wikipedia.org/wiki/Haitian_Creole_language)
     * ([ht][LanguageCode.ht]).
     */
    hat("Haitian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ht
    },

    /**
     * [Hausa](http://en.wikipedia.org/wiki/Hausa_language)
     * ([ha][LanguageCode.ha]).
     */
    hau("Hausa") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ha
    },

    /**
     * [Hawaiian](http://en.wikipedia.org/wiki/Hawaiian_language)
     *
     *
     */
    haw("Hawaiian"),

    /**
     * [Hebrew](http://en.wikipedia.org/wiki/Hebrew_language)
     * ([he][LanguageCode.he]).
     */
    heb("Hebrew") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.he
    },

    /**
     * [Herero](http://en.wikipedia.org/wiki/Herero_language)
     * ([hz][LanguageCode.hz]).
     */
    her("Herero") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.hz
    },

    /**
     * [Hiligaynon](http://en.wikipedia.org/wiki/Hiligaynon_language)
     *
     *
     */
    hil("Hiligaynon"),

    /**
     * [Himachali languages](http://en.wikipedia.org/wiki/Himachali_languages)
     *
     *
     */
    him("Himachali languages"),

    /**
     * [Hindi](http://en.wikipedia.org/wiki/Hindi)
     * ([hi][LanguageCode.hi]).
     */
    hin("Hindi") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.hi
    },

    /**
     * [Hittite](http://en.wikipedia.org/wiki/Hittite_language)
     *
     *
     */
    hit("Hittite"),

    /**
     * [Hmong](http://en.wikipedia.org/wiki/Hmong_language)
     *
     *
     */
    hmn("Hmong"),

    /**
     * [Hiri Motu](http://en.wikipedia.org/wiki/Hiri_Motu_language)
     * ([ho][LanguageCode.ho]).
     */
    hmo("Hiri Motu") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ho
    },

    /**
     * [Hmong-Mien languages](http://en.wikipedia.org/wiki/Hmong-Mien_languages)
     *
     *
     */
    hmx("Hmong-Mien languages"),

    /**
     * [Hokan languages](http://en.wikipedia.org/wiki/Hokan_languages)
     *
     *
     */
    hok("Hokan languages"),

    /**
     * [Croatian](http://en.wikipedia.org/wiki/Croatian_language)
     * ([hr][LanguageCode.hr]).
     */
    hrv("Croatian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.hr
    },

    /**
     * [Upper Sorbian](http://en.wikipedia.org/wiki/Upper_Sorbian_language)
     *
     *
     */
    hsb("Upper Sorbian"),

    /**
     * [Hungarian](http://en.wikipedia.org/wiki/Hungarian_language)
     * ([hu][LanguageCode.hu]).
     */
    hun("Hungarian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.hu
    },

    /**
     * [Hupa](http://en.wikipedia.org/wiki/Hupa_language)
     *
     *
     */
    hup("Hupa"),

    /**
     * [Armenian](http://en.wikipedia.org/wiki/Armenian_language)
     * ([hy][LanguageCode.hy]) for terminology applications.
     *
     * @see .arm
     */
    hye("Armenian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.hy


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = arm
    },

    /**
     * [Armenian](http://en.wikipedia.org/wiki/Armenian_language) (family)
     *
     *
     */
    hyx("Armenian (family)"),

    /**
     * [Iban](http://en.wikipedia.org/wiki/Iban_language)
     *
     *
     */
    iba("Iban"),

    /**
     * [Igbo](http://en.wikipedia.org/wiki/Igbo_language)
     * ([ig][LanguageCode.ig]).
     */
    ibo("Igbo") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ig
    },

    /**
     * [Icelandic](http://en.wikipedia.org/wiki/Icelandic_language)
     * ([is][LanguageCode. is]) for biblioraphic applications.
     *
     * @see .isl
     */
    ice("Icelandic") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.`is`


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = isl
    },

    /**
     * [Ido](http://en.wikipedia.org/wiki/Ido)
     * ([io][LanguageCode.io]).
     */
    ido("Ido") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.io
    },

    /**
     * [Nuosu](http://en.wikipedia.org/wiki/Nuosu_language)
     * ([ii][LanguageCode.ii]).
     */
    iii("Nuosu") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ii
    },

    /**
     * [Indo-Iranian languages](http://en.wikipedia.org/wiki/Indo-Iranian_languages)
     *
     *
     */
    iir("Indo-Iranian languages"),

    /**
     * [Ijo languages](http://en.wikipedia.org/wiki/Ijo_languages)
     *
     *
     */
    ijo("Ijo languages"),

    /**
     * [Inuktitut](http://en.wikipedia.org/wiki/Inuktitut)
     * ([iu][LanguageCode.iu]).
     */
    iku("Inuktitut") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.iu
    },

    /**
     * [Interlingue](http://en.wikipedia.org/wiki/Interlingue_language)
     * ([ie][LanguageCode.ie]).
     */
    ile("Interlingue") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ie
    },

    /**
     * [Iloko](http://en.wikipedia.org/wiki/Iloko_language)
     *
     *
     */
    ilo("Iloko"),

    /**
     * [Interlingua](http://en.wikipedia.org/wiki/Interlingua)
     * [ia][LanguageCode.ia]).
     */
    ina("Interlingua") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ia
    },

    /**
     * [Interlingua (International Auxiliary Language Association)](http://en.wikipedia.org/wiki/Interlingua_language_(International_Auxiliary_Language_Association))
     *
     *
     */
    inc("Interlingua (International Auxiliary Language Association)"),

    /**
     * [Indonesian](http://en.wikipedia.org/wiki/Indonesian_language)
     * ([id][LanguageCode.id]).
     */
    ind("Indonesian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.id
    },

    /**
     * [Indo-European languages](http://en.wikipedia.org/wiki/Indo-European_languages)
     *
     *
     */
    ine("Indo-European languages"),

    /**
     * [Ingush](http://en.wikipedia.org/wiki/Ingush_language)
     *
     *
     */
    inh("Ingush"),

    /**
     * [Inupiaq](http://en.wikipedia.org/wiki/Inupiaq_language)
     * ([ik][LanguageCode.ik]).
     */
    ipk("Inupiaq") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ik
    },

    /**
     * [Iranian languages](http://en.wikipedia.org/wiki/Iranian_languages)
     *
     *
     */
    ira("Iranian languages"),

    /**
     * [Iroquoian languages](http://en.wikipedia.org/wiki/Iroquoian_languages)
     *
     *
     */
    iro("Iroquoian languages"),

    /**
     * [Icelandic](http://en.wikipedia.org/wiki/Icelandic_language)
     * ([is][LanguageCode. is]) for terminology applications.
     *
     * @see .ice
     */
    isl("Icelandic") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.`is`


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = ice
    },

    /**
     * [Italian](http://en.wikipedia.org/wiki/Italian_language)
     * ([it][LanguageCode.it]).
     */
    ita("Italian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.it
    },

    /**
     * [Italic languages](http://en.wikipedia.org/wiki/Italic_languages)
     *
     *
     */
    itc("Italic languages"),

    /**
     * [Javanese](http://en.wikipedia.org/wiki/Javanese_language)
     * ([jv][LanguageCode.jv]).
     */
    jav("Javanese") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.jv
    },

    /**
     * [Lojban](http://en.wikipedia.org/wiki/Lojban_language)
     *
     *
     */
    jbo("Lojban"),

    /**
     * [Japanese](http://en.wikipedia.org/wiki/Japanese_language)
     * ([ja][LanguageCode.ja]).
     */
    jpn("Japanese") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ja
    },

    /**
     * [Judeo-Persian](http://en.wikipedia.org/wiki/Judeo-Persian_language)
     *
     *
     */
    jpr("Judeo-Persian"),

    /**
     * [Japanese](http://en.wikipedia.org/wiki/Japanese_language) (family)
     *
     *
     */
    jpx("Japanese (family)"),

    /**
     * [Judeo-Arabic](http://en.wikipedia.org/wiki/Judeo-Arabic_language)
     *
     *
     */
    jrb("Judeo-Arabic"),

    /**
     * [Kara-Kalpak](http://en.wikipedia.org/wiki/Kara-Kalpak_language)
     *
     *
     */
    kaa("Kara-Kalpak"),

    /**
     * [Kabyle](http://en.wikipedia.org/wiki/Kabyle_language)
     *
     *
     */
    kab("Kabyle"),

    /**
     * [Kachin](http://en.wikipedia.org/wiki/Kachin_language)
     *
     *
     */
    kac("Kachin"),

    /**
     * [Kalaallisut](http://en.wikipedia.org/wiki/Kalaallisut_language)
     * ([kl][LanguageCode.kl]).
     */
    kal("Kalaallisut") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.kl
    },

    /**
     * [Kamba (Kenya)](http://en.wikipedia.org/wiki/Kamba_language_(Kenya))
     *
     *
     */
    kam("Kamba (Kenya)"),

    /**
     * [Kannada](http://en.wikipedia.org/wiki/Kannada_language)
     * ([kn][LanguageCode.kn]).
     */
    kan("Kannada") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.kn
    },

    /**
     * [Karen languages](http://en.wikipedia.org/wiki/Karen_languages)
     *
     *
     */
    kar("Karen languages"),

    /**
     * [Kashmiri](http://en.wikipedia.org/wiki/Kashmiri_language)
     * ([ks][LanguageCode.ks]).
     */
    kas("Kashmiri") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ks
    },

    /**
     * [Georgian](http://en.wikipedia.org/wiki/Georgian_language)
     * ([ka][LanguageCode.ka]) for terminology applications.
     *
     * @see .geo
     */
    kat("Georgian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ka


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = geo
    },

    /**
     * [Kanuri](http://en.wikipedia.org/wiki/Kanuri_language)
     * ([kr][LanguageCode.kr]).
     */
    kau("Kanuri") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.kr
    },

    /**
     * [Kawi](http://en.wikipedia.org/wiki/Kawi_language)
     *
     *
     */
    kaw("Kawi"),

    /**
     * [Kazakh](http://en.wikipedia.org/wiki/Kazakh_language)
     * ([kk][LanguageCode.kk]).
     */
    kaz("Kazakh") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.kk
    },

    /**
     * [Kabardian](http://en.wikipedia.org/wiki/Kabardian_language)
     *
     *
     */
    kbd("Kabardian"),

    /**
     * [Kordofanian languages](http://en.wikipedia.org/wiki/Kordofanian_languages)
     *
     *
     */
    kdo("Kordofanian languages"),

    /**
     * [Khasi](http://en.wikipedia.org/wiki/Khasi_language)
     *
     *
     */
    kha("Khasi"),

    /**
     * [Khoisan languages](http://en.wikipedia.org/wiki/Khoisan_languages)
     *
     *
     */
    khi("Khoisan languages"),

    /**
     * [Khmer](http://en.wikipedia.org/wiki/Khmer_language)
     * ([km][LanguageCode.km]).
     */
    khm("Central Khmer") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.km
    },

    /**
     * [Khotanese](http://en.wikipedia.org/wiki/Khotanese_language)
     *
     *
     */
    kho("Khotanese"),

    /**
     * [Kikuyu](http://en.wikipedia.org/wiki/Gikuyu_language)
     * ([ki][LanguageCode.ki]).
     */
    kik("Kikuyu") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ki
    },

    /**
     * [Kinyarwanda](http://en.wikipedia.org/wiki/Kinyarwanda)
     * ([rw][LanguageCode.rw]).
     */
    kin("Kinyarwanda") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.rw
    },

    /**
     * [Kyrgyz](http://en.wikipedia.org/wiki/Kyrgyz_language)
     * ([ky][LanguageCode.ky]).
     */
    kir("Kirghiz") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ky
    },

    /**
     * [Kimbundu](http://en.wikipedia.org/wiki/Kimbundu_language)
     *
     *
     */
    kmb("Kimbundu"),

    /**
     * [Konkani](http://en.wikipedia.org/wiki/Konkani_language) (macrolanguage)
     *
     *
     */
    kok("Konkani"),

    /**
     * [Komi](http://en.wikipedia.org/wiki/Komi_language)
     * ([kv][LanguageCode.kv]).
     */
    kom("Komi") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.kv
    },

    /**
     * [Kongo](http://en.wikipedia.org/wiki/Kongo_language)
     * ([kg][LanguageCode.kg]).
     */
    kon("Kongo") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.kg
    },

    /**
     * [Korean](http://en.wikipedia.org/wiki/Korean_language)
     * ([ko][LanguageCode.ko]).
     */
    kor("Korean") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ko
    },

    /**
     * [Kosraean](http://en.wikipedia.org/wiki/Kosraean_language)
     *
     *
     */
    kos("Kosraean"),

    /**
     * [Kpelle](http://en.wikipedia.org/wiki/Kpelle_language)
     *
     *
     */
    kpe("Kpelle"),

    /**
     * [Karachay-Balkar](http://en.wikipedia.org/wiki/Karachay-Balkar_language)
     *
     *
     */
    krc("Karachay-Balkar"),

    /**
     * [Karelian](http://en.wikipedia.org/wiki/Karelian_language)
     *
     *
     */
    krl("Karelian"),

    /**
     * [Kru languages](http://en.wikipedia.org/wiki/Kru_languages)
     *
     *
     */
    kro("Kru languages"),

    /**
     * [Kurukh](http://en.wikipedia.org/wiki/Kurukh_language)
     *
     *
     */
    kru("Kurukh"),

    /**
     * [Kwanyama](http://en.wikipedia.org/wiki/Kwanyama)
     * ([kj][LanguageCode.kj]).
     */
    kua("Kuanyama") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.kj
    },

    /**
     * [Kumyk](http://en.wikipedia.org/wiki/Kumyk_language)
     *
     *
     */
    kum("Kumyk"),

    /**
     * [Kurdish](http://en.wikipedia.org/wiki/Kurdish_language)
     * ([ku][LanguageCode.ku]).
     */
    kur("Kurdish") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ku
    },

    /**
     * [Kutenai](http://en.wikipedia.org/wiki/Kutenai_language)
     *
     *
     */
    kut("Kutenai"),

    /**
     * [Ladino](http://en.wikipedia.org/wiki/Ladino_language)
     *
     *
     */
    lad("Ladino"),

    /**
     * [Lahnda](http://en.wikipedia.org/wiki/Lahnda_language)
     *
     *
     */
    lah("Lahnda"),

    /**
     * [Lamba](http://en.wikipedia.org/wiki/Lamba_language)
     *
     *
     */
    lam("Lamba"),

    /**
     * [Lao](http://en.wikipedia.org/wiki/Lao_language)
     * ([lo][LanguageCode.lo]).
     */
    lao("Lao") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.lo
    },

    /**
     * [Latin](http://en.wikipedia.org/wiki/Latin)
     * ([la][LanguageCode.la]).
     */
    lat("Latin") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.la
    },

    /**
     * [Latvian](http://en.wikipedia.org/wiki/Latvian_language)
     * ([lv][LanguageCode.lv]).
     */
    lav("Latvian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.lv
    },

    /**
     * [Lezghian](http://en.wikipedia.org/wiki/Lezghian_language)
     *
     *
     */
    lez("Lezghian"),

    /**
     * [Limburgish](http://en.wikipedia.org/wiki/Limburgish_language)
     * ([li][LanguageCode.li]).
     */
    lim("Limburgan") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.li
    },

    /**
     * [Lingala](http://en.wikipedia.org/wiki/Lingala_language)
     * ([ln][LanguageCode.ln]).
     */
    lin("Lingala") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ln
    },

    /**
     * [Lithuanian](http://en.wikipedia.org/wiki/Lithuanian_language)
     * ([lt][LanguageCode.lt]).
     */
    lit("Lithuanian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.lt
    },

    /**
     * [Mongo](http://en.wikipedia.org/wiki/Mongo_language)
     *
     *
     */
    lol("Mongo"),

    /**
     * [Lozi](http://en.wikipedia.org/wiki/Lozi_language)
     *
     *
     */
    loz("Lozi"),

    /**
     * [
 * Luxembourgish](http://en.wikipedia.org/wiki/Luxembourgish_language)
     * ([lb][LanguageCode.lb]).
     */
    ltz("Luxembourgish") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.lb
    },

    /**
     * [Luba-Lulua](http://en.wikipedia.org/wiki/Luba-Lulua_language)
     *
     *
     */
    lua("Luba-Lulua"),

    /**
     * [Luba-Katanga](http://en.wikipedia.org/wiki/Tshiluba_language)
     * ([lu][LanguageCode.lu]).
     */
    lub("Luba-Katanga") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.lu
    },

    /**
     * [Ganda](http://en.wikipedia.org/wiki/Luganda)
     * ([lg][LanguageCode.lg]).
     */
    lug("Ganda") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.lg
    },

    /**
     * [Luiseno](http://en.wikipedia.org/wiki/Luiseno_language)
     *
     *
     */
    lui("Luiseno"),

    /**
     * [Lunda](http://en.wikipedia.org/wiki/Lunda_language)
     *
     *
     */
    lun("Lunda"),

    /**
     * [Luo (Kenya and Tanzania)](http://en.wikipedia.org/wiki/Luo_language_(Kenya_and_Tanzania))
     *
     *
     */
    luo("Luo (Kenya and Tanzania)"),

    /**
     * [Lushai](http://en.wikipedia.org/wiki/Lushai_language)
     *
     *
     */
    lus("Lushai"),

    /**
     * [Macedonian](http://en.wikipedia.org/wiki/Macedonian_language)
     * ([mk][LanguageCode.mk]) for bibliographic applications.
     *
     * @see .mkd
     */
    mac("Macedonian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.mk


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = mkd
    },

    /**
     * [Madurese](http://en.wikipedia.org/wiki/Madurese_language)
     *
     *
     */
    mad("Madurese"),

    /**
     * [Magahi](http://en.wikipedia.org/wiki/Magahi_language)
     *
     *
     */
    mag("Magahi"),

    /**
     * [Marshallese](http://en.wikipedia.org/wiki/Marshallese_language)
     * ([mh][LanguageCode.mh]).
     */
    mah("Marshallese") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.mh
    },

    /**
     * [Maithili](http://en.wikipedia.org/wiki/Maithili_language)
     *
     *
     */
    mai("Maithili"),

    /**
     * [Makasar](http://en.wikipedia.org/wiki/Makasar_language)
     *
     *
     */
    mak("Makasar"),

    /**
     * [Malayalam](http://en.wikipedia.org/wiki/Malayalam_language)
     * ([ml][LanguageCode.ml]).
     */
    mal("Malayalam") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ml
    },

    /**
     * [Mandingo](http://en.wikipedia.org/wiki/Manding_languages)
     *
     *
     */
    man("Mandingo"),

    /**
     * [M&#257;ori](http://en.wikipedia.org/wiki/M%C4%81ori_language)
     * ([mi][LanguageCode.mi]) for bibliographic applications.
     *
     * @see .mri
     */
    mao("M\u0101ori") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.mi


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = mri
    },

    /**
     * [Austronesian languages](http://en.wikipedia.org/wiki/Austronesian_languages)
     *
     *
     */
    map("Austronesian languages"),

    /**
     * [Marathi](http://en.wikipedia.org/wiki/Marathi_language)
     * ([mr][LanguageCode.mr]).
     */
    mar("Marathi") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.mr
    },

    /**
     * [Masai](http://en.wikipedia.org/wiki/Masai_language)
     *
     *
     */
    mas("Masai"),

    /**
     * [Malay](http://en.wikipedia.org/wiki/Malay_language) (macrolanguage)
     * ([ms][LanguageCode.ms]) for bibliographic applications.
     *
     * @see .msa
     */
    may("Malay") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ms


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = msa
    },

    /**
     * [Moksha](http://en.wikipedia.org/wiki/Moksha_language)
     *
     *
     */
    mdf("Moksha"),

    /**
     * [Mandar](http://en.wikipedia.org/wiki/Mandar_language)
     *
     *
     */
    mdr("Mandar"),

    /**
     * [Mende (Sierra Leone)](http://en.wikipedia.org/wiki/Mende_language_(Sierra_Leone))
     *
     *
     */
    men("Mende (Sierra Leone)"),

    /**
     * [Middle Irish](http://en.wikipedia.org/wiki/Middle_Irish_language) (900-1200)
     *
     *
     */
    mga("Middle Irish"),

    /**
     * [Mi'kmaq](http://en.wikipedia.org/wiki/Mi%27kmaq_language)
     *
     *
     */
    mic("Mi'kmaq"),

    /**
     * [Minangkabau](http://en.wikipedia.org/wiki/Minangkabau_language)
     *
     *
     */
    min("Minangkabau"),

    /**
     * Uncoded languages.
     *
     *
     */
    mis("Uncoded languages"),

    /**
     * [Macedonian](http://en.wikipedia.org/wiki/Macedonian_language)
     * ([mk][LanguageCode.mk]) for terminology applications.
     *
     * @see .mac
     */
    mkd("Macedonian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.mk


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = mac
    },

    /**
     * [Mon-Khmer languages](http://en.wikipedia.org/wiki/Mon-Khmer_languages)
     *
     *
     */
    mkh("Mon-Khmer languages"),

    /**
     * [Malagasy](http://en.wikipedia.org/wiki/Malagasy_language)
     * ([mg][LanguageCode.mg]).
     */
    mlg("Malagasy") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.mg
    },

    /**
     * [Maltese](http://en.wikipedia.org/wiki/Maltese_language)
     * ([mt][LanguageCode.mt]).
     */
    mlt("Maltese") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.mt
    },

    /**
     * [Manchu](http://en.wikipedia.org/wiki/Manchu_language)
     *
     *
     */
    mnc("Manchu"),

    /**
     * [Manipuri](http://en.wikipedia.org/wiki/Manipuri_language)
     *
     *
     */
    mni("Manipuri"),

    /**
     * [Manobo languages](http://en.wikipedia.org/wiki/Manobo_languages)
     *
     *
     */
    mno("Manobo languages"),

    /**
     * [Mohawk](http://en.wikipedia.org/wiki/Mohawk_language)
     *
     *
     */
    moh("Mohawk"),

    /**
     * [Mongolian](http://en.wikipedia.org/wiki/Mongolian_language)
     * ([mn][LanguageCode.mn]).
     */
    mon("Mongolian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.mn
    },

    /**
     * [Mossi](http://en.wikipedia.org/wiki/Mossi_language)
     *
     *
     */
    mos("Mossi"),

    /**
     * [M&#257;ori](http://en.wikipedia.org/wiki/M%C4%81ori_language)
     * ([mi][LanguageCode.mi]) for terminology applications.
     *
     * @see .mao
     */
    mri("M\u0101ori") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.mi


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = mao
    },

    /**
     * [Malay](http://en.wikipedia.org/wiki/Malay_language)
     * ([ms][LanguageCode.ms]) for terminology applications.
     *
     * @see .may
     */
    msa("Malay") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ms


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = may
    },

    /**
     * Multiple languages.
     *
     *
     */
    mul("Multiple languages"),

    /**
     * [Munda languages](http://en.wikipedia.org/wiki/Munda_languages)
     *
     *
     */
    mun("Munda languages"),

    /**
     * [Creek](http://en.wikipedia.org/wiki/Creek_language)
     *
     *
     */
    mus("Creek"),

    /**
     * [Mirandese](http://en.wikipedia.org/wiki/Mirandese_language)
     *
     *
     */
    mwl("Mirandese"),

    /**
     * [Marwari](http://en.wikipedia.org/wiki/Marwari_language)
     *
     *
     */
    mwr("Marwari"),

    /**
     * [Burmese](http://en.wikipedia.org/wiki/Burmese_language)
     * ([my][LanguageCode.my]) for terminology applications.
     *
     * @see .bur
     */
    mya("Burmese") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.my


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = bur
    },

    /**
     * [Mayan languages](http://en.wikipedia.org/wiki/Mayan_languages)
     *
     *
     */
    myn("Mayan languages"),

    /**
     * [Erzya](http://en.wikipedia.org/wiki/Erzya_language)
     *
     *
     */
    myv("Erzya"),

    /**
     * [Nahuatl languages](http://en.wikipedia.org/wiki/Nahuatl_languages)
     *
     *
     */
    nah("Nahuatl languages"),

    /**
     * [North American Indian](http://en.wikipedia.org/wiki/North_American_Indian_languages)
     *
     *
     */
    nai("North American Indian"),

    /**
     * [Neapolitan](http://en.wikipedia.org/wiki/Neapolitan_language)
     *
     *
     */
    nap("Neapolitan"),

    /**
     * [Nauru](http://en.wikipedia.org/wiki/Nauruan_language)
     * ([na][LanguageCode.na]).
     */
    nau("Nauru") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.na
    },

    /**
     * [Navajo](http://en.wikipedia.org/wiki/Navajo_language)
     * ([nv][LanguageCode.nv]).
     */
    nav("Navajo") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.nv
    },

    /**
     * [Southern
 * Ndebele](http://en.wikipedia.org/wiki/Southern_Ndebele_language)
     * ([nr][LanguageCode.nr]).
     */
    nbl("South Ndebele") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.nr
    },

    /**
     * [Northern
 * Ndebele](http://en.wikipedia.org/wiki/Northern_Ndebele_language)
     * ([nd][LanguageCode.nd]).
     */
    nde("North Ndebele") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.nd
    },

    /**
     * [Ndonga](http://en.wikipedia.org/wiki/Ndonga)
     * ([ng][LanguageCode.ng]).
     */
    ndo("Ndonga") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ng
    },

    /**
     * [Low German](http://en.wikipedia.org/wiki/Low_German_language)
     *
     *
     */
    nds("Low German"),

    /**
     * [Nepali](http://en.wikipedia.org/wiki/Nepali_language) (macrolanguage)
     * ([ne][LanguageCode.ne]).
     */
    nep("Nepali") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ne
    },

    /**
     * [Newari](http://en.wikipedia.org/wiki/Newari_language)
     *
     *
     *
     * Because `new` is a special word for Java programming
     * language, `new` cannot be used as an enum entry.
     * So, the first letter of this entry is a capital letter.
     *
     *
     *
     *
     * `toString()` method of this instance (`New`)
     * returns `"new"`.
     *
     *
     *
     */
    New("Newari") {
        override fun toString(): String {
            return "new"
        }
    },

    /**
     * [Trans-New Guinea languages](http://en.wikipedia.org/wiki/Trans-New_Guinea_languages)
     *
     *
     */
    ngf("Trans-New Guinea languages"),

    /**
     * [Nias](http://en.wikipedia.org/wiki/Nias_language)
     *
     *
     */
    nia("Nias"),

    /**
     * [Niger-Kordofanian languages](http://en.wikipedia.org/wiki/Niger-Kordofanian_languages)
     *
     *
     */
    nic("Niger-Kordofanian languages"),

    /**
     * [Niuean](http://en.wikipedia.org/wiki/Niuean_language)
     *
     *
     */
    niu("Niuean"),

    /**
     * [Dutch](http://en.wikipedia.org/wiki/Dutch_language)
     * ([nl][LanguageCode.nl]) for terminology applications.
     *
     * @see .dut
     */
    nld("Dutch") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.nl


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = dut
    },

    /**
     * [Norwegian Nynorsk](http://en.wikipedia.org/wiki/Nynorsk)
     * ([nn][LanguageCode.nn]).
     */
    nno("Norwegian Nynorsk") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.nn
    },

    /**
     * [Norwegian
 * Bokml](http://en.wikipedia.org/wiki/Bokm%C3%A5l)
     * ([nb][LanguageCode.nb]).
     */
    nob("Norwegian Bokm\u00E5l") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.nb
    },

    /**
     * [Nogai](http://en.wikipedia.org/wiki/Nogai_language)
     *
     *
     */
    nog("Nogai"),

    /**
     * [Old Norse](http://en.wikipedia.org/wiki/Old_Norse_language)
     *
     *
     */
    non("Old Norse"),

    /**
     * [Norwegian](http://en.wikipedia.org/wiki/Norwegian_language)
     * ([no][LanguageCode.no]).
     */
    nor("Norwegian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.no
    },

    /**
     * [N'Ko](http://en.wikipedia.org/wiki/N%27Ko_language)
     *
     *
     */
    nqo("N'Ko"),

    /**
     * [Pedi](http://en.wikipedia.org/wiki/Pedi_language)
     *
     *
     */
    nso("Pedi"),

    /**
     * [Nubian languages](http://en.wikipedia.org/wiki/Nubian_languages)
     *
     *
     */
    nub("Nubian languages"),

    /**
     * [Classical Newari](http://en.wikipedia.org/wiki/Classical_Newari_language)
     *
     *
     */
    nwc("Classical Newari"),

    /**
     * [Chichewa](http://en.wikipedia.org/wiki/Chichewa_language)
     * ([ny][LanguageCode.ny]).
     */
    nya("Nyanja") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ny
    },

    /**
     * [Nyamwezi](http://en.wikipedia.org/wiki/Nyamwezi_language)
     *
     *
     */
    nym("Nyamwezi"),

    /**
     * [Nyankole](http://en.wikipedia.org/wiki/Nyankole_language)
     *
     *
     */
    nyn("Nyankole"),

    /**
     * [Nyoro](http://en.wikipedia.org/wiki/Nyoro_language)
     *
     *
     */
    nyo("Nyoro"),

    /**
     * [Nzima](Nzima)
     *
     *
     */
    nzi("Nzima"),

    /**
     * [Occitan](http://en.wikipedia.org/wiki/Occitan_language) (post 1500)
     * ([oc][LanguageCode.oc]).
     */
    oci("Occitan") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.oc
    },

    /**
     * [Ojibwe](http://en.wikipedia.org/wiki/Ojibwe_language)
     * ([oj][LanguageCode.oj]).
     */
    oji("Ojibwa") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.oj
    },

    /**
     * [Oto-Manguean languages](http://en.wikipedia.org/wiki/Oto-Manguean_languages)
     *
     *
     */
    omq("Oto-Manguean languages"),

    /**
     * [Omotic languages](http://en.wikipedia.org/wiki/Omotic_languages)
     *
     *
     */
    omv("Omotic languages"),

    /**
     * [Oriya](http://en.wikipedia.org/wiki/Oriya_language) (macrolanguage)
     * ([or][LanguageCode.or]).
     */
    ori("Oriya") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.or
    },

    /**
     * [Oromo](http://en.wikipedia.org/wiki/Oromo_language)
     * ([om][LanguageCode.om]).
     */
    orm("Oromo") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.om
    },

    /**
     * [Osage](http://en.wikipedia.org/wiki/Osage_language)
     *
     *
     */
    osa("Osage"),

    /**
     * [Ossetian](http://en.wikipedia.org/wiki/Ossetic_language)
     * ([os][LanguageCode.os]).
     */
    oss("Ossetian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.os
    },

    /**
     * [Ottoman Turkish](http://en.wikipedia.org/wiki/Ottoman_Turkish_language) (1500-1928)
     *
     *
     */
    ota("Ottoman Turkish"),

    /**
     * [Otomian languages](http://en.wikipedia.org/wiki/Otomian_languages)
     *
     *
     */
    oto("Otomian languages"),

    /**
     * [Papuan languages](http://en.wikipedia.org/wiki/Papuan_languages)
     *
     *
     */
    paa("Papuan languages"),

    /**
     * [Pangasinan](http://en.wikipedia.org/wiki/Pangasinan_language)
     *
     *
     */
    pag("Pangasinan"),

    /**
     * [Pahlavi](http://en.wikipedia.org/wiki/Pahlavi_language)
     *
     *
     */
    pal("Pahlavi"),

    /**
     * [Pampanga](http://en.wikipedia.org/wiki/Pampanga_language)
     *
     *
     */
    pam("Pampanga"),

    /**
     * [Punjabi](http://en.wikipedia.org/wiki/Punjabi_language)
     * ([pa][LanguageCode.pa]).
     */
    pan("Panjabi") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.pa
    },

    /**
     * [Papiamento](http://en.wikipedia.org/wiki/Papiamento_language)
     *
     *
     */
    pap("Papiamento"),

    /**
     * [Palauan](http://en.wikipedia.org/wiki/Palauan_language)
     *
     *
     */
    pau("Palauan"),

    /**
     * [Old Persian](http://en.wikipedia.org/wiki/Old_Persian_language) (ca. 600-400 B.C.)
     */
    peo("Old Persian"),

    /**
     * [Persian](http://en.wikipedia.org/wiki/Persian_language)
     * ([fa][LanguageCode.fa]) for bibliographic applications.
     *
     * @see .fas
     */
    per("Persian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.fa


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = fas
    },

    /**
     * [Philippine languages](http://en.wikipedia.org/wiki/Philippine_languages)
     *
     *
     */
    phi("Philippine languages"),

    /**
     * [Phoenician](http://en.wikipedia.org/wiki/Phoenician_language)
     *
     *
     */
    phn("Phoenician"),

    /**
     * [Central Malayo-Polynesian languages](http://en.wikipedia.org/wiki/Central_Malayo-Polynesian_languages)
     *
     *
     */
    plf("Central Malayo-Polynesian languages"),

    /**
     * [P&#257;li](http://en.wikipedia.org/wiki/P%C4%81li_language)
     * ([pi][LanguageCode.pi]).
     */
    pli("P\u0101li") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.pi
    },

    /**
     * [Polish](http://en.wikipedia.org/wiki/Polish_language)
     * ([pl][LanguageCode.pl]).
     */
    pol("Polish") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.pl
    },

    /**
     * [Pohnpeian](http://en.wikipedia.org/wiki/Pohnpeian_language)
     *
     *
     */
    pon("Pohnpeian"),

    /**
     * [Portuguese](http://en.wikipedia.org/wiki/Portuguese_language)
     * ([pt][LanguageCode.pt]).
     */
    por("Portuguese") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.pt
    },

    /**
     * [Malayo-Polynesian languages](http://en.wikipedia.org/wiki/Malayo-Polynesian_languages)
     *
     *
     */
    poz("Malayo-Polynesian languages"),

    /**
     * [Eastern Malayo-Polynesian languages](http://en.wikipedia.org/wiki/Eastern_Malayo-Polynesian_languages)
     *
     *
     */
    pqe("Eastern Malayo-Polynesian languages"),

    /**
     * [Western Malayo-Polynesian languages](http://en.wikipedia.org/wiki/Western_Malayo-Polynesian_languages)
     *
     *
     */
    pqw("Western Malayo-Polynesian languages"),

    /**
     * [Prakrit languages](http://en.wikipedia.org/wiki/Prakrit_languages)
     *
     *
     */
    pra("Prakrit languages"),

    /**
     * [Old Provenal]() (to 1500)
     *
     *
     */
    pro("Old Proven\u00E7al"),

    /**
     * [Pashto](http://en.wikipedia.org/wiki/Pashto_language)
     * ([ps][LanguageCode.ps]).
     */
    pus("Pushto") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ps
    },

    /**
     * [Quechua](http://en.wikipedia.org/wiki/Quechua_language)
     * ([qu][LanguageCode.qu]).
     */
    que("Quechua") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.qu
    },

    /**
     * [Quechuan](http://en.wikipedia.org/wiki/Quechuan_language) (family)
     *
     *
     */
    qwe("Quechuan (family)"),

    /**
     * [Rajasthani](http://en.wikipedia.org/wiki/Rajasthani_language)
     *
     *
     */
    raj("Rajasthani"),

    /**
     * [Rapanui](http://en.wikipedia.org/wiki/Rapanui_language)
     *
     *
     */
    rap("Rapanui"),

    /**
     * [Rarotongan](http://en.wikipedia.org/wiki/Rarotongan_language)
     *
     *
     */
    rar("Rarotongan"),

    /**
     * [Romance languages](http://en.wikipedia.org/wiki/Romance_languages)
     *
     *
     */
    roa("Romance languages"),

    /**
     * [Romansh](http://en.wikipedia.org/wiki/Romansh_language)
     * ([rm][LanguageCode.rm])
     */
    roh("Romansh") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.rm
    },

    /**
     * [Romany](http://en.wikipedia.org/wiki/Romany_language)
     *
     *
     */
    rom("Romany"),

    /**
     * [Romanian](http://en.wikipedia.org/wiki/Romanian_language)
     * ([ro][LanguageCode.ro]) for terminology applications.
     *
     * @see .rum
     */
    ron("Romanian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ro


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = rum
    },

    /**
     * [Romanian](http://en.wikipedia.org/wiki/Romanian_language)
     * ([ro][LanguageCode.ro]) for bibliographic applications.
     *
     * @see .ron
     */
    rum("Romansh") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ro


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = ron
    },

    /**
     * [Kirundi](http://en.wikipedia.org/wiki/Kirundi)
     * ([rn][LanguageCode.rn]).
     */
    run("Kirundi") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.rn
    },

    /**
     * [Macedo-Romanian](http://en.wikipedia.org/wiki/Macedo-Romanian_language)
     *
     *
     */
    rup("Macedo-Romanian"),

    /**
     * [Russian](http://en.wikipedia.org/wiki/Russian_language)
     * ([ru][LanguageCode.ru]).
     */
    rus("Russian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ru
    },

    /**
     * [Sango](http://en.wikipedia.org/wiki/Sango_language)
     *
     *
     */
    sad("Sango"),

    /**
     * [Sango](http://en.wikipedia.org/wiki/Sango_language)
     * ([sg][LanguageCode.sg]).
     */
    sag("Sango") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.sg
    },

    /**
     * [Yakut](http://en.wikipedia.org/wiki/Yakut_language)
     *
     *
     */
    sah("Yakut"),

    /**
     * [South American Indian languages](http://en.wikipedia.org/wiki/South_American_Indian_languages)
     *
     *
     */
    sai("South American Indian languages"),

    /**
     * [Salishan languages](http://en.wikipedia.org/wiki/Salishan_languages)
     *
     *
     */
    sal("Salishan languages"),

    /**
     * [Samaritan Aramaic](http://en.wikipedia.org/wiki/Samaritan_Aramaic_language)
     *
     *
     */
    sam("Samaritan Aramaic"),

    /**
     * [Sanskrit](http://en.wikipedia.org/wiki/Sanskrit)
     * ([sa][LanguageCode.sa]).
     */
    san("Sanskrit") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.sa
    },

    /**
     * [Sasak](http://en.wikipedia.org/wiki/Sasak_language)
     *
     *
     */
    sas("Sasak"),

    /**
     * [Santali](http://en.wikipedia.org/wiki/Santali_language)
     *
     *
     */
    sat("Santali"),

    /**
     * [Sicilian](http://en.wikipedia.org/wiki/Sicilian_language)
     *
     *
     */
    scn("Sicilian"),

    /**
     * [Scots](http://en.wikipedia.org/wiki/Scots_language)
     *
     *
     */
    sco("Scots"),

    /**
     * [Eastern Sudanic languages](http://en.wikipedia.org/wiki/Eastern_Sudanic_languages)
     *
     *
     */
    sdv("Eastern Sudanic languages"),

    /**
     * [Selkup](http://en.wikipedia.org/wiki/Selkup_language)
     *
     *
     */
    sel("Selkup"),

    /**
     * [Semitic languages](http://en.wikipedia.org/wiki/Semitic_languages)
     *
     *
     */
    sem("Semitic languages"),

    /**
     * [Old Irish](http://en.wikipedia.org/wiki/Old_Irish_language) (to 900)
     *
     *
     */
    sga("Old Irish"),

    /**
     * [Sign languages](http://en.wikipedia.org/wiki/Sign_languages)
     *
     *
     */
    sgn("Sign languages"),

    /**
     * [Shan](http://en.wikipedia.org/wiki/Shan_language)
     *
     *
     */
    shn("Shan"),

    /**
     * [Sidamo](http://en.wikipedia.org/wiki/Sidamo_language)
     *
     *
     */
    sid("Sidamo"),

    /**
     * [Sinhala](http://en.wikipedia.org/wiki/Sinhala_language)
     * ([si][LanguageCode.si]).
     */
    sin("Sinhala") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.si
    },

    /**
     * [Siouan languages](http://en.wikipedia.org/wiki/Siouan_languages)
     *
     *
     */
    sio("Siouan languages"),

    /**
     * [Sino-Tibetan languages](http://en.wikipedia.org/wiki/Sino-Tibetan_languages)
     *
     *
     */
    sit("Sino-Tibetan languages"),

    /**
     * [Slavic languages](http://en.wikipedia.org/wiki/Slavic_languages)
     *
     *
     */
    sla("Slavic languages"),

    /**
     * [Slovak](http://en.wikipedia.org/wiki/Slovak_language)
     * ([sk][LanguageCode.sk]) for terminology aplications.
     *
     * @see .slo
     */
    slk("Slovak") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.sk


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = slo
    },

    /**
     * [Slovak](http://en.wikipedia.org/wiki/Slovak_language)
     * ([sk][LanguageCode.sk]) for bibliographic aplications.
     *
     * @see .slk
     */
    slo("Slovak") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.sk


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = slk
    },

    /**
     * [Slovene](http://en.wikipedia.org/wiki/Slovene_language)
     * ([sl][LanguageCode.sl]).
     */
    slv("Slovene") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.sl
    },

    /**
     * [Southern Sami](http://en.wikipedia.org/wiki/Southern_Sami_language)
     *
     *
     */
    sma("Southern Sami"),

    /**
     * [Northern Sami](http://en.wikipedia.org/wiki/Northern_Sami)
     * ([se][LanguageCode.se]).
     */
    sme("Northern Sami") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.se
    },

    /**
     * [Sami languages](http://en.wikipedia.org/wiki/Sami_languages)
     *
     *
     */
    smi("Sami languages"),

    /**
     * [Lule Sami](http://en.wikipedia.org/wiki/Lule_Sami_language)
     *
     *
     */
    smj("Lule Sami"),

    /**
     * [Inari Sami](http://en.wikipedia.org/wiki/Inari_Sami_language)
     *
     *
     */
    smn("Inari Sami"),

    /**
     * [Samoan](http://en.wikipedia.org/wiki/Samoan_language)
     * ([sm][LanguageCode.sm]).
     */
    smo("Samoan") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.sm
    },

    /**
     * [Skolt Sami](http://en.wikipedia.org/wiki/Skolt_Sami_language)
     *
     *
     */
    sms("Skolt Sami"),

    /**
     * [Shona](http://en.wikipedia.org/wiki/Shona_language)
     * ([sn][LanguageCode.sn]).
     */
    sna("Shona") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.sn
    },

    /**
     * [Sindhi](http://en.wikipedia.org/wiki/Sindhi_language)
     * ([sd][LanguageCode.sd]).
     */
    snd("Sindhi") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.sd
    },

    /**
     * [Soninke](http://en.wikipedia.org/wiki/Soninke_language)
     *
     *
     */
    snk("Soninke"),

    /**
     * [Sogdian](http://en.wikipedia.org/wiki/Sogdian_language)
     *
     *
     */
    sog("Sogdian"),

    /**
     * [Somali](http://en.wikipedia.org/wiki/Somali_language)
     * ([so][LanguageCode.so]).
     */
    som("Somali") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.so
    },

    /**
     * [Songhai languages](http://en.wikipedia.org/wiki/Songhai_languages)
     *
     *
     */
    son("Songhai languages"),

    /**
     * [Southern Sotho](http://en.wikipedia.org/wiki/Sotho_language)
     * ([st][LanguageCode.st]).
     */
    sot("Southern Sotho") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.st
    },

    /**
     * [Spanish](http://en.wikipedia.org/wiki/Spanish_language)
     * ([es][LanguageCode.es]).
     */
    spa("Spanish") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.es
    },

    /**
     * [Albanian](http://en.wikipedia.org/wiki/Albanian_language)
     * ([sq][LanguageCode.sq]) for terminology applications.
     *
     * @see .alb
     */
    sqi("Albanian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.sq


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = alb
    },

    /**
     * [Albanian languages](http://en.wikipedia.org/wiki/Albanian_languages)
     *
     *
     */
    sqj("Albanian languages"),

    /**
     * [Sardinian](http://en.wikipedia.org/wiki/Sardinian_language)
     * ([sc][LanguageCode.sc]).
     */
    srd("Sardinian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.sc
    },

    /**
     * [Sranan Tongo](http://en.wikipedia.org/wiki/Sranan_Tongo_language)
     *
     *
     */
    srn("Sranan Tongo"),

    /**
     * [Serbian](http://en.wikipedia.org/wiki/Serbian_language)
     * ([sr][LanguageCode.sr]).
     */
    srp("Serbian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.sr
    },

    /**
     * [Serer](http://en.wikipedia.org/wiki/Serer_language)
     *
     *
     */
    srr("Serer"),

    /**
     * [Nilo-Saharan languages](http://en.wikipedia.org/wiki/Nilo-Saharan_languages)
     *
     *
     */
    ssa("Nilo-Saharan languages"),

    /**
     * [Swati](http://en.wikipedia.org/wiki/Swati_language)
     * ([ss][LanguageCode.ss]).
     */
    ssw("Swati") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ss
    },

    /**
     * [Sukuma](http://en.wikipedia.org/wiki/Sukuma_language)
     *
     *
     */
    suk("Sukuma"),

    /**
     * [Sundanese](http://en.wikipedia.org/wiki/Sundanese_language)
     * ([su][LanguageCode.su]).
     */
    sun("Sundanese") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.su
    },

    /**
     * [Susu](http://en.wikipedia.org/wiki/Susu_language)
     *
     *
     */
    sus("Susu"),

    /**
     * [Sumerian](http://en.wikipedia.org/wiki/Sumerian_language)
     *
     *
     */
    sux("Sumerian"),

    /**
     * [Swahili](http://en.wikipedia.org/wiki/Swahili_language) (macrolanguage)
     * ([sw][LanguageCode.sw]).
     */
    swa("Swahili") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.sw
    },

    /**
     * [Swedish](http://en.wikipedia.org/wiki/Swedish_language)
     * ([sv][LanguageCode.sv]).
     */
    swe("Swedish") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.sv
    },

    /**
     * [Classical Syriac](http://en.wikipedia.org/wiki/Classical_Syriac_language)
     *
     *
     */
    syc("Classical Syriac"),

    /**
     * [Samoyedic languages](http://en.wikipedia.org/wiki/Samoyedic_languages)
     *
     *
     */
    syd("Samoyedic languages"),

    /**
     * [Syriac](http://en.wikipedia.org/wiki/Syriac_language)
     *
     *
     */
    syr("Syriac"),

    /**
     * [Tahitian](http://en.wikipedia.org/wiki/Tahitian_language)
     * ([ty][LanguageCode.ty]).
     */
    tah("Tahitian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ty
    },

    /**
     * [Tai languages](http://en.wikipedia.org/wiki/Tai_languages)
     *
     *
     */
    tai("Tai languages"),

    /**
     * [Tamil](http://en.wikipedia.org/wiki/Tamil_language)
     * ([ta][LanguageCode.ta]).
     */
    tam("Tamil") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ta
    },

    /**
     * [Tatar](http://en.wikipedia.org/wiki/Tatar_language)
     * ([tt][LanguageCode.tt]).
     */
    tat("Tatar") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.tt
    },

    /**
     * [Tibeto-Burman languages](http://en.wikipedia.org/wiki/Tibeto-Burman_languages)
     *
     *
     */
    tbq("Tibeto-Burman languages"),

    /**
     * [Telugu](http://en.wikipedia.org/wiki/Telugu_language)
     * ([te][LanguageCode.te]).
     */
    tel("Telugu") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.te
    },

    /**
     * [Timne](http://en.wikipedia.org/wiki/Timne_language)
     *
     *
     */
    tem("Timne"),

    /**
     * [Tereno](http://en.wikipedia.org/wiki/Tereno_language)
     *
     *
     */
    ter("Tereno"),

    /**
     * [Tetum](http://en.wikipedia.org/wiki/Tetum_language)
     *
     *
     */
    tet("Tetum"),

    /**
     * [Tajik](http://en.wikipedia.org/wiki/Tajik_language)
     * ([tg][LanguageCode.tg]).
     */
    tgk("Tajik") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.tg
    },

    /**
     * [Tagalog](http://en.wikipedia.org/wiki/Tagalog_language)
     * ([tl][LanguageCode.tl]).
     */
    tgl("Tagalog") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.tl
    },

    /**
     * [Thai](http://en.wikipedia.org/wiki/Thai_language)
     * ([th][LanguageCode.th]).
     */
    tha("Thai") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.th
    },

    /**
     * [Tibetan](http://en.wikipedia.org/wiki/Standard_Tibetan)
     * ([bo][LanguageCode.bo]) for terminology applications.
     *
     * @see .bod
     */
    tib("Tibetan") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.bo


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = bod
    },

    /**
     * [Tigre](http://en.wikipedia.org/wiki/Tigre_language)
     *
     *
     */
    tig("Tigre"),

    /**
     * [Tigrinya](http://en.wikipedia.org/wiki/Tigrinya_language)
     * ([ti][LanguageCode.ti]).
     */
    tir("Tigrinya") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ti
    },

    /**
     * [Tiv](http://en.wikipedia.org/wiki/Tiv_language)
     *
     *
     */
    tiv("Tiv"),

    /**
     * [Tokelau](http://en.wikipedia.org/wiki/Tokelau_language)
     *
     *
     */
    tkl("Tokelau"),

    /**
     * [Klingon](http://en.wikipedia.org/wiki/Klingon_language)
     *
     *
     */
    tlh("Klingon"),

    /**
     * [Tlingit](http://en.wikipedia.org/wiki/Tlingit_language)
     *
     *
     */
    tli("Tlingit"),

    /**
     * [Tamashek](http://en.wikipedia.org/wiki/Tamashek_language)
     *
     *
     */
    tmh("Tamashek"),

    /**
     * [Tonga (Nyasa)](http://en.wikipedia.org/wiki/Tonga_language_(Nyasa))
     *
     *
     */
    tog("Tonga (Nyasa)"),

    /**
     * [Tonga (Tonga Islands)](http://en.wikipedia.org/wiki/Tonga_language_(Tonga_Islands))
     * ([to][LanguageCode.to]).
     */
    ton("Tonga (Tonga Islands)") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.to
    },

    /**
     * [Tok Pisin](http://en.wikipedia.org/wiki/Tok_Pisin_language)
     *
     *
     */
    tpi("Tok Pisin"),

    /**
     * [Turkic languages](http://en.wikipedia.org/wiki/Turkic_languages)
     *
     *
     */
    trk("Turkic languages"),

    /**
     * [Tsimshian](http://en.wikipedia.org/wiki/Tsimshian_language)
     *
     *
     */
    tsi("Tsimshian"),

    /**
     * [Tswana](http://en.wikipedia.org/wiki/Tswana_language)
     * ([tn][LanguageCode.tn]).
     */
    tsn("Tswana") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.tn
    },

    /**
     * [Tsonga](http://en.wikipedia.org/wiki/Tsonga_language)
     * ([ts][LanguageCode.ts]).
     */
    tso("Tsonga") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ts
    },

    /**
     * [Turkmen](http://en.wikipedia.org/wiki/Turkmen_language)
     * ([tk][LanguageCode.tk]).
     */
    tuk("Turkmen") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.tk
    },

    /**
     * [Tumbuka](http://en.wikipedia.org/wiki/Tumbuka_language)
     *
     *
     */
    tum("Tumbuka"),

    /**
     * [Tupi languages](http://en.wikipedia.org/wiki/Tupi_languages)
     *
     *
     */
    tup("Tupi languages"),

    /**
     * [Turkish](http://en.wikipedia.org/wiki/Turkish_language)
     * ([tr][LanguageCode.tr]).
     */
    tur("Turkish") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.tr
    },

    /**
     * [Altaic languages](http://en.wikipedia.org/wiki/Altaic_languages)
     *
     *
     */
    tut("Altaic languages"),

    /**
     * [Tungus languages](http://en.wikipedia.org/wiki/Tungus_languages)
     *
     *
     */
    tuw("Tungus languages"),

    /**
     * [Tuvalu](http://en.wikipedia.org/wiki/Tuvalu_language)
     *
     *
     */
    tvl("Tuvalu"),

    /**
     * [Twi](http://en.wikipedia.org/wiki/Twi)
     * ([tw][LanguageCode.tw]).
     */
    twi("Twi") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.tw
    },

    /**
     * [Tuvinian](http://en.wikipedia.org/wiki/Tuvinian_language)
     *
     *
     */
    tyv("Tuvinian"),

    /**
     * [Udmurt](http://en.wikipedia.org/wiki/Udmurt_language)
     *
     *
     */
    udm("Udmurt"),

    /**
     * [Ugaritic](http://en.wikipedia.org/wiki/Ugaritic_language)
     *
     *
     */
    uga("Ugaritic"),

    /**
     * [Uighur](http://en.wikipedia.org/wiki/Uyghur_language)
     * ([ug][LanguageCode.ug]).
     */
    uig("Uighur") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ug
    },

    /**
     * [Ukrainian](http://en.wikipedia.org/wiki/Ukrainian_language)
     * ([uk][LanguageCode.uk]).
     */
    ukr("Ukrainian") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.uk
    },

    /**
     * [Umbundu](http://en.wikipedia.org/wiki/Umbundu_language)
     *
     *
     */
    umb("Umbundu"),

    /**
     * Undetermined.
     *
     *
     */
    und("Undetermined"),

    /**
     * [Urdu](http://en.wikipedia.org/wiki/Urdu)
     * ([ur][LanguageCode.ur]).
     */
    urd("Urdu") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ur
    },

    /**
     * [Uralic languages](http://en.wikipedia.org/wiki/Uralic_languages)
     *
     *
     */
    urj("Uralic languages"),

    /**
     * [Uzbek](http://en.wikipedia.org/wiki/Uzbek_language)
     * ([uz][LanguageCode.uz]).
     */
    uzb("Uzbek") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.uz
    },

    /**
     * [Vai](http://en.wikipedia.org/wiki/Vai_language)
     *
     *
     */
    vai("Vai"),

    /**
     * [Venda](http://en.wikipedia.org/wiki/Venda_language)
     * ([ve][LanguageCode.ve]).
     */
    ven("Venda") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.ve
    },

    /**
     * [Vietnamese](http://en.wikipedia.org/wiki/Vietnamese_language)
     * ([vi][LanguageCode.vi]).
     */
    vie("Vietnamese") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.vi
    },

    /**
     * [Volapk](http://en.wikipedia.org/wiki/Volap%C3%BCk)
     * ([vo][LanguageCode.vo]).
     */
    vol("Volap\u00FCk") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.vo
    },

    /**
     * [Votic](http://en.wikipedia.org/wiki/Votic_language)
     *
     *
     */
    vot("Votic"),

    /**
     * [Wakashan languages](http://en.wikipedia.org/wiki/Wakashan_languages)
     *
     *
     */
    wak("Wakashan languages"),

    /**
     * [Wolaytta](http://en.wikipedia.org/wiki/Wolaytta_language)
     *
     *
     */
    wal("Wolaytta"),

    /**
     * [Waray (Philippines)](http://en.wikipedia.org/wiki/Waray_language_(Philippines))
     *
     *
     */
    war("Waray (Philippines)"),

    /**
     * [Washo](http://en.wikipedia.org/wiki/Washo_language)
     *
     *
     */
    was("Washo"),

    /**
     * [Welsh](http://en.wikipedia.org/wiki/Welsh_language)
     * ([cy][LanguageCode.cy]) for bibliographic applications.
     *
     * @see .cym
     */
    wel("Welsh") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.cy


        override val usage: Usage
            get() = Usage.BIBLIOGRAPHY


        override val synonym: LanguageAlpha3Code
            get() = cym
    },

    /**
     * [Sorbian languages](http://en.wikipedia.org/wiki/Sorbian_languages)
     *
     *
     */
    wen("Sorbian languages"),

    /**
     * [Walloon](http://en.wikipedia.org/wiki/Walloon_language)
     * ([wa][LanguageCode.wa]).
     */
    wln("Walloon") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.wa
    },

    /**
     * [Wolof](http://en.wikipedia.org/wiki/Wolof_language)
     * ([wo][LanguageCode.wo]).
     */
    wol("Wolof") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.wo
    },

    /**
     * [Kalmyk](http://en.wikipedia.org/wiki/Kalmyk_language)
     *
     *
     */
    xal("Kalmyk"),

    /**
     * [Mongolian languages](http://en.wikipedia.org/wiki/Mongolian_languages)
     *
     *
     */
    xgn("Mongolian languages"),

    /**
     * [Xhosa](http://en.wikipedia.org/wiki/Xhosa_language)
     * ([xh][LanguageCode.xh]).
     */
    xho("Xhosa") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.xh
    },

    /**
     * [Na-Dene languages](http://en.wikipedia.org/wiki/Na-Dene_languages)
     *
     *
     */
    xnd("Na-Dene languages"),

    /**
     * [Yao](http://en.wikipedia.org/wiki/Yao_language)
     *
     *
     */
    yao("Yao"),

    /**
     * [Yapese](http://en.wikipedia.org/wiki/Yapese_language)
     *
     *
     */
    yap("Yapese"),

    /**
     * [Yiddish](http://en.wikipedia.org/wiki/Yiddish_language)
     * ([yi][LanguageCode.yi]).
     */
    yid("Yiddish") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.yi
    },

    /**
     * [Yoruba](http://en.wikipedia.org/wiki/Yoruba_language)
     * ([yo][LanguageCode.yo]).
     */
    yor("Yoruba") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.yo
    },

    /**
     * [Yupik languages](http://en.wikipedia.org/wiki/Yupik_languages)
     *
     *
     */
    ypk("Yupik languages"),

    /**
     * [Zapotec](http://en.wikipedia.org/wiki/Zapotec_language)
     *
     *
     */
    zap("Zapotec"),

    /**
     * [Blissymbols](http://en.wikipedia.org/wiki/Blissymbols_language)
     *
     *
     */
    zbl("Blissymbols"),

    /**
     * [Zenaga](http://en.wikipedia.org/wiki/Zenaga_language)
     *
     *
     */
    zen("Zenaga"),

    /**
     * [Zhuang](http://en.wikipedia.org/wiki/Zhuang_languages)
     * ([za][LanguageCode.za]).
     */
    zha("Zhuang") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.za
    },

    /**
     * [Chinese](http://en.wikipedia.org/wiki/Chinese_language)
     * ([zh][LanguageCode.zh]) for terminology applications.
     *
     * @see .chi
     */
    zho("Chinese") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.zh


        override val usage: Usage
            get() = Usage.TERMINOLOGY


        override val synonym: LanguageAlpha3Code
            get() = chi
    },

    /**
     * [Chinese](http://en.wikipedia.org/wiki/Chinese_language) (family)
     *
     *
     */
    zhx("Chinese (family)"),

    /**
     * [East Slavic languages](http://en.wikipedia.org/wiki/East_Slavic_languages)
     *
     *
     */
    zle("East Slavic languages"),

    /**
     * [South Slavic languages](http://en.wikipedia.org/wiki/South_Slavic_languages)
     *
     *
     */
    zls("South Slavic languages"),

    /**
     * [West Slavic languages](http://en.wikipedia.org/wiki/West_Slavic_languages)
     *
     *
     */
    zlw("West Slavic languages"),

    /**
     * [Zande languages](http://en.wikipedia.org/wiki/Zande_languages)
     *
     *
     */
    znd("Zande languages"),

    /**
     * [Zulu](http://en.wikipedia.org/wiki/Zulu_language)
     * ([zu][LanguageCode.zu]).
     */
    zul("Zulu") {
        override val alpha2: LanguageCode?
            get() = LanguageCode.zu
    },

    /**
     * [Zuni](http://en.wikipedia.org/wiki/Zuni_language)
     *
     *
     */
    zun("Zuni"),

    /**
     * No linguistic content.
     *
     *
     */
    zxx("No linguistic content"),

    /**
     * [Zaza](http://en.wikipedia.org/wiki/Zaza_language)
     *
     *
     */
    zza("Zaza");


    /**
     * Get [ISO 639-1](http://en.wikipedia.org/wiki/ISO_639-1)
     * language code.
     *
     * @return
     * [ISO 639-1](http://en.wikipedia.org/wiki/ISO_639-1)
     * language code. This method may return `null`.
     */
    open val alpha2: LanguageCode?
        get() = null


    /**
     * Get the usage of this language code.
     *
     *
     *
     * Most language codes return [Usage.COMMON].
     *
     *
     * @return
     * The usage of this language code.
     */
    open val usage: Usage
        get() = Usage.COMMON


    /**
     * Get the synonym of this code.
     *
     *
     *
     * In most cases, this method simply returns `this` object. On
     * the other hand, language codes that have two alpha-3 codes, namely, ISO
     * 639-2/T code ("terminological" code) and ISO 639-2/B code
     * ("bibliographic" code), return their counterparts. For example,
     * [LanguageAlpha3Code.deu][.deu]`.getSynonym()` returns
     * [LanguageAlpha3Code.ger][.ger].
     *
     *
     * @return
     * [ISO 639-2](http://en.wikipedia.org/wiki/ISO_639-2)
     * language code.
     */
    open val synonym: LanguageAlpha3Code
        get() = this


    /**
     * Get the bibliographic code of this language.
     *
     *
     *
     * Most languages have just one [ISO 639-2](http://en.wikipedia.org/wiki/ISO_639-2) code and they
     * simply return `this` object. Only ISO 639-2/T codes
     * ("terminological" codes) return counterpart objects. For example,
     * [LanguageAlpha3Code.fra]`.getAlpha3B()`
     * returns [LanguageAlpha3Code.fre].
     *
     *
     * @return
     * The bibliographic code of this language.
     */
    val alpha3B: LanguageAlpha3Code
        get() = if (usage == Usage.BIBLIOGRAPHY) {
            this
        } else {
            synonym
        }


    /**
     * Get the terminological code of this language.
     *
     *
     *
     * Most languages have just one [ISO 639-2](http://en.wikipedia.org/wiki/ISO_639-2) code and they
     * simply return `this` object. Only ISO 639-2/B codes
     * ("bibliographic" codes) return counterpart objects. For example,
     * [LanguageAlpha3Code.fre]`.getAlpha3T()`
     * returns [LanguageAlpha3Code.fra].
     *
     *
     *
     * @return
     * The terminological code of this language.
     */
    val alpha3T: LanguageAlpha3Code
        get() = if (usage == Usage.TERMINOLOGY) {
            this
        } else {
            synonym
        }


    /**
     * The usage of this language code.
     *
     *
     */
    enum class Usage {
        /**
         * Code for terminology applications (ISO 639-2/T).
         */
        TERMINOLOGY,

        /**
         * Code for bibliographic applications (ISO 639-2/B).
         */
        BIBLIOGRAPHY,

        /**
         * For all applications including both terminology and
         * bibliographic applications.
         */
        COMMON
    }

    companion object {


        /**
         * Get a `LanguageAlpha3Code` that corresponds to a given
         * [ISO 639-1](http://en.wikipedia.org/wiki/ISO_639-1) code
         * (2-letter lowercase code) or
         * [ISO 639-2](http://en.wikipedia.org/wiki/ISO_639-2) code
         * (3-letter lowercase code).
         *
         *
         *
         * This method calls [getByCode][.getByCode]`(code, false)`.
         *
         *
         * @param code
         * An [ISO 639-1](http://en.wikipedia.org/wiki/ISO_639-1)
         * code (2-letter lowercase code) or an
         * [ISO 639-2](http://en.wikipedia.org/wiki/ISO_639-2)
         * code (3-letter lowercase code). Or "undefined" (case insensitive).
         * Note that if the given code is one of legacy language codes
         * ("iw", "ji" and "in"), it is treated as its official counterpart
         * ("he", "yi" and "id"), respectively. For example, if "in" is
         * given, this method returns [LanguageAlpha3Code.ind][.ind].
         *
         * @return
         * A `LanguageAlpha3Code` instance, or `null` if not found.
         * If [ISO 639-1](http://en.wikipedia.org/wiki/ISO_639-1)
         * code (2-letter code) is given and the language has two
         * [ISO 639-2](http://en.wikipedia.org/wiki/ISO_639-2)
         * codes, ISO 639/T code ("terminological" code) is returned.
         *
         *
         */
        fun getByCodeIgnoreCase(code: String): LanguageAlpha3Code? {
            return getByCode(code, false)
        }


        /**
         * Get a `LanguageAlpha3Code` that corresponds to a given
         * [ISO 639-1](http://en.wikipedia.org/wiki/ISO_639-1) code
         * (2-letter lowercase code) or
         * [ISO 639-2](http://en.wikipedia.org/wiki/ISO_639-2) code
         * (3-letter lowercase code).
         *
         * @param code
         * An [ISO 639-1](http://en.wikipedia.org/wiki/ISO_639-1)
         * code (2-letter lowercase code) or an
         * [ISO 639-2](http://en.wikipedia.org/wiki/ISO_639-2)
         * code (3-letter lowercase code). Or "undefined" (its case
         * sensitivity depends on the value of `caseSensitive`).
         * Note that if the given code is one of legacy language codes
         * ("iw", "ji" and "in"), it is treated as its official counterpart
         * ("he", "yi" and "id"), respectively. For example, if "in" is
         * given, this method returns [LanguageAlpha3Code.ind][.ind].
         *
         * @param caseSensitive
         * If `true`, the given code should consist of lowercase letters only.
         * If `false`, this method internally canonicalizes the given code by
         * [String.toLowerCase] and then performs search. For
         * example, `getByCode("JPN", true)` returns `null`, but on the
         * other hand, `getByCode("JPN", false)` returns [         LanguageAlpha3Code.jpn][.jpn].
         *
         * As an exceptional case, both `getByCode("New", true)` and
         * `getByCode("new", true)` return [.New] (Newari).
         *
         * @return
         * A `LanguageAlpha3Code` instance, or `null` if not found.
         * If [ISO 639-1](http://en.wikipedia.org/wiki/ISO_639-1)
         * code (2-letter code) is given and the language has two
         * [ISO 639-2](http://en.wikipedia.org/wiki/ISO_639-2)
         * codes, ISO 639/T code ("terminological" code) is returned.
         */
        @JvmOverloads
        @JvmStatic
        fun getByCode(code: String?, caseSensitive: Boolean = true): LanguageAlpha3Code? {
            val canonicalizeCode = canonicalize(code, caseSensitive) ?: return null

            when (canonicalizeCode.length) {
                2 -> {}

                3, 9 -> return getByEnumName(canonicalizeCode)

                else -> return null
            }

            val canonicalizeLanguageCode = LanguageCode.canonicalize(canonicalizeCode, caseSensitive) ?: ""
            val alpha2 = LanguageCode.getByEnumName(canonicalizeLanguageCode) ?: return null

            return alpha2.alpha3
        }

        @JvmStatic
        fun getByEnumName(name: String): LanguageAlpha3Code? {
            try {
                return LanguageAlpha3Code.valueOf(name)
            } catch (e: IllegalArgumentException) {
                return null
            }

        }


        private fun canonicalize(code: String?, caseSensitive: Boolean): String? {
            if (code == null || code.isEmpty()) return null

            var canonicalizedCode = if (!caseSensitive) code.toLowerCase() else code

            // A special case for Newari.
            canonicalizedCode = if (code == "new") "New" else canonicalizedCode

            return canonicalizedCode
        }


        /**
         * Get a list of `LanguageAlpha3Code` by a languageName regular expression.
         *
         *
         *
         * This method is almost equivalent to [ findByName][.findByName]`(Pattern.compile(regex))`.
         *
         *
         * @param regex
         * Regular expression for names.
         *
         * @return
         * List of `LanguageAlpha3Code`. If nothing has matched,
         * an empty list is returned.
         *
         * @throws IllegalArgumentException
         * `regex` is `null`.
         *
         * @throws java.util.regex.PatternSyntaxException
         * `regex` failed to be compiled.
         *
         *
         */
        @JvmStatic
        fun findByName(regex: String?): List<LanguageAlpha3Code> {
            if (regex == null) {
                throw IllegalArgumentException("regex is null.")
            }

            // Compile the regular expression. This may throw
            // java.util.regex.PatternSyntaxException.
            val pattern = Pattern.compile(regex)

            return findByName(pattern)
        }


        /**
         * Get a list of `LanguageAlpha3Code` by a languageName pattern.
         *
         *
         *
         * For example, the list obtained by the code snippet below:
         *
         *
         * <pre style="background-color: #EEEEEE; margin-left: 2em; margin-right: 2em; border: 1px solid black; padding: 0.5em;">
         * Pattern pattern = Pattern.compile(<span style="color: darkred;">"Old.*"</span>);
         * List&lt;LanguageAlpha3Code&gt; list = LanguageAlpha3Code.findByName(pattern);</pre>
         *
         *
         *
         * contains 7 `LanguageAlpha3Code`s as listed below.
         *
         *
         *
         *  1. [.ang] : Old English
         *  1. [.fro] : Old French
         *  1. [.goh] : Old High German
         *  1. [.non] : Old Norse
         *  1. [.peo] : Old Persian
         *  1. [.pro] : Old Provenal
         *  1. [.sga] : Old Irish
         *
         *
         * @param pattern
         * Pattern to match names.
         *
         * @return
         * List of `LanguageAlpha3Code`. If nothing has matched,
         * an empty list is returned.
         *
         * @throws IllegalArgumentException
         * `pattern` is `null`.
         *
         *
         */
        fun findByName(pattern: Pattern?): List<LanguageAlpha3Code> {
            if (pattern == null) {
                throw IllegalArgumentException("pattern is null.")
            }

            val list = ArrayList<LanguageAlpha3Code>()

            for (entry in values()) {
                if (pattern.matcher(entry.languageName).matches()) {
                    list.add(entry)
                }
            }

            return list
        }
    }
}
/**
 * Get a `LanguageAlpha3Code` that corresponds to a given
 * [ISO 639-1](http://en.wikipedia.org/wiki/ISO_639-1) code
 * (2-letter lowercase code) or
 * [ISO 639-2](http://en.wikipedia.org/wiki/ISO_639-2) code
 * (3-letter lowercase code).
 *
 *
 *
 * This method calls [getByCode][.getByCode]`(code, true)`.
 * this method was an alias of `getByCode(code, false)`.
 *
 *
 * @param code
 * An [ISO 639-1](http://en.wikipedia.org/wiki/ISO_639-1)
 * code (2-letter lowercase code) or an
 * [ISO 639-2](http://en.wikipedia.org/wiki/ISO_639-2)
 * code (3-letter lowercase code). Or "undefined".
 * Note that if the given code is one of legacy language codes
 * ("iw", "ji" and "in"), it is treated as its official counterpart
 * ("he", "yi" and "id"), respectively. For example, if "in" is
 * given, this method returns [LanguageAlpha3Code.ind][.ind].
 *
 * @return
 * A `LanguageAlpha3Code` instance, or `null` if not found.
 * If [ISO 639-1](http://en.wikipedia.org/wiki/ISO_639-1)
 * code (2-letter code) is given and the language has two
 * [ISO 639-2](http://en.wikipedia.org/wiki/ISO_639-2)
 * codes, ISO 639/T code ("terminological" code) is returned.
 */
