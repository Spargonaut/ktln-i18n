/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.spargonaut.i18n

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertSame
import org.junit.jupiter.api.Assertions.assertTrue
import org.spargonaut.i18n.LanguageCode.Companion.getByCode


import org.spargonaut.i18n.LanguageCode.Companion.getByLocale
import java.util.Locale


class LanguageCodeTest {
    @Test
    fun `should produce the appropriate language code when given the language code as a lowercase string`() {
        assertSame(LanguageCode.he, getByCode("he"))
        assertSame(LanguageCode.id, getByCode("id"))
        assertSame(LanguageCode.id, getByCode("in"))
        assertSame(LanguageCode.he, getByCode("iw"))
        assertSame(LanguageCode.ja, getByCode("ja"))
        assertSame(LanguageCode.yi, getByCode("ji"))
        assertSame(LanguageCode.yi, getByCode("yi"))
    }

    @Test
    fun `should produce null when given the language code as an uppercase string`() {
        assertNull(getByCode("JA"))
    }

    @Test
    fun `should produce the appropriate language code when given the language code as a lowercase string and case sensitive`() {
        assertSame(LanguageCode.ja, getByCode("ja", true))
    }

    @Test
    fun `should produce null when given the language code as an uppercase string and case sensitive`() {
        assertNull(getByCode("JA", true))
    }

    @Test
    fun `should produce the appropriate language code when given the language code as a lowercase string and case insensitive`() {
        assertSame(LanguageCode.ja, getByCode("ja", false))
    }

    @Test
    fun `should produce the appropriate language code when given the language code as a uppercase string and case insensitive`() {
        assertSame(LanguageCode.ja, getByCode("JA", false))
    }

    @Test
    fun `should produce null when given null`() {
        assertNull(getByCode(null))
    }

    @Test
    fun `should produce null when given an empty string`() {
        assertNull(getByCode(""))
    }

    @Test
    fun `should produce null when given a question mark`() {
        assertNull(getByCode("?"))
    }

    @Test
    fun `should produce null when given two question marks`() {
        assertNull(getByCode("??"))
    }

    @Test
    fun `should produce null when given three question marks`() {
        assertNull(getByCode("???"))
    }

    @Test
    fun `should produce null when given four question marks`() {
        assertNull(getByCode("????"))
    }

    @Test
    fun `should produce the locale language code`() {
        assertSame(Locale.CHINESE, LanguageCode.zh.toLocale())
        assertSame(Locale.ENGLISH, LanguageCode.en.toLocale())
        assertSame(Locale.FRENCH, LanguageCode.fr.toLocale())
        assertSame(Locale.GERMAN, LanguageCode.de.toLocale())
        assertSame(Locale.ITALIAN, LanguageCode.it.toLocale())
        assertSame(Locale.JAPANESE, LanguageCode.ja.toLocale())
        assertSame(Locale.KOREAN, LanguageCode.ko.toLocale())
    }

    @Test
    fun `should produce null when getting the locale with null`() {
        assertNull(getByLocale(null))
    }

    @Test
    fun `should produce the Undefined language code when getting the LanguageCode by Locale with an empty string`() {
        assertSame(LanguageCode.undefined, getByLocale(Locale("")))
    }

    @Test
    fun `should produce the appropriate language code when getting the LanguageCode by Locale`() {
        assertSame(LanguageCode.de, getByLocale(Locale.GERMAN))
        assertSame(LanguageCode.en, getByLocale(Locale.ENGLISH))
        assertSame(LanguageCode.fr, getByLocale(Locale.CANADA_FRENCH))
        assertSame(LanguageCode.fr, getByLocale(Locale.FRENCH))
        assertSame(LanguageCode.it, getByLocale(Locale.ITALIAN))
        assertSame(LanguageCode.ja, getByLocale(Locale.JAPANESE))
        assertSame(LanguageCode.ko, getByLocale(Locale.KOREAN))
        assertSame(LanguageCode.zh, getByLocale(Locale.CHINESE))
        assertSame(LanguageCode.zh, getByLocale(Locale.SIMPLIFIED_CHINESE))
        assertSame(LanguageCode.zh, getByLocale(Locale.TRADITIONAL_CHINESE))
    }

    @Test
    fun `should produce the appropriate language code when given a Locale object`() {
        assertSame(LanguageCode.ja, getByLocale(Locale("ja")))
        assertSame(LanguageCode.ja, getByLocale(Locale("ja", "JP")))
    }

    @Test
    fun `should produce the appropriate language code when given a string as two character code`() {
        assertSame(LanguageCode.bo, getByCode("bo"))
        assertSame(LanguageCode.cs, getByCode("cs"))
        assertSame(LanguageCode.cy, getByCode("cy"))
        assertSame(LanguageCode.de, getByCode("de"))
        assertSame(LanguageCode.el, getByCode("el"))
        assertSame(LanguageCode.eu, getByCode("eu"))
        assertSame(LanguageCode.fa, getByCode("fa"))
        assertSame(LanguageCode.fr, getByCode("fr"))
        assertSame(LanguageCode.hy, getByCode("hy"))
        assertSame(LanguageCode.`is`, getByCode("is"))
        assertSame(LanguageCode.ka, getByCode("ka"))
        assertSame(LanguageCode.mi, getByCode("mi"))
        assertSame(LanguageCode.mk, getByCode("mk"))
        assertSame(LanguageCode.ms, getByCode("ms"))
        assertSame(LanguageCode.my, getByCode("my"))
        assertSame(LanguageCode.nl, getByCode("nl"))
        assertSame(LanguageCode.ro, getByCode("ro"))
        assertSame(LanguageCode.sk, getByCode("sk"))
        assertSame(LanguageCode.sq, getByCode("sq"))
    }

    @Test
    fun `should produce the appropriate language code when given a string as three character code`() {
        assertSame(LanguageCode.sq, getByCode("alb"))
        assertSame(LanguageCode.hy, getByCode("arm"))
        assertSame(LanguageCode.eu, getByCode("baq"))
        assertSame(LanguageCode.bo, getByCode("bod"))
        assertSame(LanguageCode.my, getByCode("bur"))
        assertSame(LanguageCode.cs, getByCode("ces"))
        assertSame(LanguageCode.cy, getByCode("cym"))
        assertSame(LanguageCode.cs, getByCode("cze"))
        assertSame(LanguageCode.nl, getByCode("dut"))
        assertSame(LanguageCode.eu, getByCode("eus"))
        assertSame(LanguageCode.eu, getByCode("eus"))
        assertSame(LanguageCode.de, getByCode("ger"))
        assertSame(LanguageCode.de, getByCode("deu"))
        assertSame(LanguageCode.el, getByCode("ell"))
        assertSame(LanguageCode.fa, getByCode("fas"))
        assertSame(LanguageCode.fr, getByCode("fra"))
        assertSame(LanguageCode.fr, getByCode("fre"))
        assertSame(LanguageCode.el, getByCode("gre"))
        assertSame(LanguageCode.hy, getByCode("hye"))
        assertSame(LanguageCode.`is`, getByCode("ice"))
        assertSame(LanguageCode.`is`, getByCode("isl"))
        assertSame(LanguageCode.ka, getByCode("kat"))
        assertSame(LanguageCode.ka, getByCode("geo"))
        assertSame(LanguageCode.mk, getByCode("mac"))
        assertSame(LanguageCode.mi, getByCode("mao"))
        assertSame(LanguageCode.ms, getByCode("may"))
        assertSame(LanguageCode.mk, getByCode("mkd"))
        assertSame(LanguageCode.mi, getByCode("mri"))
        assertSame(LanguageCode.ms, getByCode("msa"))
        assertSame(LanguageCode.my, getByCode("mya"))
        assertSame(LanguageCode.nl, getByCode("nld"))
        assertSame(LanguageCode.fa, getByCode("per"))
        assertSame(LanguageCode.ro, getByCode("ron"))
        assertSame(LanguageCode.ro, getByCode("rum"))
        assertSame(LanguageCode.sk, getByCode("slk"))
        assertSame(LanguageCode.sk, getByCode("slo"))
        assertSame(LanguageCode.sq, getByCode("sqi"))
        assertSame(LanguageCode.bo, getByCode("tib"))
        assertSame(LanguageCode.cy, getByCode("wel"))
    }

    @Test
    fun `should produce an appropriate language code when given the LanguageAlpha3Code of a LanguageCode`() {
        assertSame(LanguageAlpha3Code.bod, LanguageCode.bo.alpha3)
        assertSame(LanguageAlpha3Code.ces, LanguageCode.cs.alpha3)
        assertSame(LanguageAlpha3Code.cym, LanguageCode.cy.alpha3)
        assertSame(LanguageAlpha3Code.deu, LanguageCode.de.alpha3)
        assertSame(LanguageAlpha3Code.ell, LanguageCode.el.alpha3)
        assertSame(LanguageAlpha3Code.eus, LanguageCode.eu.alpha3)
        assertSame(LanguageAlpha3Code.fas, LanguageCode.fa.alpha3)
        assertSame(LanguageAlpha3Code.fra, LanguageCode.fr.alpha3)
        assertSame(LanguageAlpha3Code.hye, LanguageCode.hy.alpha3)
        assertSame(LanguageAlpha3Code.isl, LanguageCode.`is`.alpha3)
        assertSame(LanguageAlpha3Code.jpn, LanguageCode.ja.alpha3)
        assertSame(LanguageAlpha3Code.kat, LanguageCode.ka.alpha3)
        assertSame(LanguageAlpha3Code.mri, LanguageCode.mi.alpha3)
        assertSame(LanguageAlpha3Code.mkd, LanguageCode.mk.alpha3)
        assertSame(LanguageAlpha3Code.msa, LanguageCode.ms.alpha3)
        assertSame(LanguageAlpha3Code.mya, LanguageCode.my.alpha3)
        assertSame(LanguageAlpha3Code.nld, LanguageCode.nl.alpha3)
        assertSame(LanguageAlpha3Code.ron, LanguageCode.ro.alpha3)
        assertSame(LanguageAlpha3Code.slk, LanguageCode.sk.alpha3)
        assertSame(LanguageAlpha3Code.sqi, LanguageCode.sq.alpha3)
    }

    @Test
    fun `should produce the language as a proper case string from the languageName of a LanguageCode`() {
        assertSame("Japanese", LanguageCode.ja.languageName)
    }

    @Test
    fun `should produce a list of Language codes when given a regular expression as a string`() {
        val list = LanguageCode.findByName(".*nese")

        assertEquals(5, list.size)

        // an : Aragonese
        assertTrue(list.contains(LanguageCode.an))

        // ja : Japanese
        assertTrue(list.contains(LanguageCode.ja))

        // jv : Javanese
        assertTrue(list.contains(LanguageCode.jv))

        // su : Sudanese
        assertTrue(list.contains(LanguageCode.su))

        // zh : Chinese
        assertTrue(list.contains(LanguageCode.zh))
    }

    @Test
    fun `should produce the 'undefined' LanguageCode when given "undefined" as an all lowercase code string`() {
        assertSame(LanguageCode.undefined, getByCode("undefined"))
    }

    @Test
    fun `should produce a null LanguageCode when given "undefined" as an all uppercase code string`() {
        assertNull(getByCode("UNDEFINED"))
    }

    @Test
    fun `should produce a null LanguageCode when given "undefined" as an all uppercase code string with case sensitivity turned off`() {
        assertSame(LanguageCode.undefined, getByCode("UNDEFINED", false))
    }
}
