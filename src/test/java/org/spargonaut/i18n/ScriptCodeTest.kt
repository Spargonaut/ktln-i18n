/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.spargonaut.i18n


import io.kotlintest.matchers.string.contain
import io.kotlintest.should
import io.kotlintest.shouldThrow
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertSame
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import org.junit.platform.suite.api.IncludeEngines
import org.spargonaut.i18n.ScriptCode.Companion.getByCode
import org.spargonaut.i18n.ScriptCode.Companion.getByCodeIgnoreCase
import java.security.SecureRandom
import kotlin.experimental.and


class ScriptCodeTest {

    @Test
    fun `should get a script code by case sensitivity By Default`() {
        assertSame(ScriptCode.Jpan, getByCode("Jpan"))
    }

    @Test
    fun `should get a null script code by default for casings other than proper case`() {
        assertNull(getByCode("JPan"))
        assertNull(getByCode("JpAn"))
        assertNull(getByCode("JpaN"))
        assertNull(getByCode("jpan"))
        assertNull(getByCode("jPan"))
        assertNull(getByCode("jpAn"))
        assertNull(getByCode("jpaN"))
    }

    @Test
    fun `should get the proper script code when case sensitivity is on explicitly`() {
        assertSame(ScriptCode.Jpan, getByCode("Jpan", true))
    }

    @Test
    fun `should get a null script code for casings other than proper case when case sensitivity is on explicitily`() {
        assertNull(getByCode("JPan", true))
        assertNull(getByCode("JpAn", true))
        assertNull(getByCode("JpaN", true))
        assertNull(getByCode("jpan", true))
        assertNull(getByCode("jPan", true))
        assertNull(getByCode("jpAn", true))
        assertNull(getByCode("jpaN", true))
    }

    @Test
    fun `should get a null script code for a null code`() {
        assertNull(getByCode(null))
    }

    @Test
    fun `should get a null script code for an empty string code`() {
        assertNull(getByCode(""))
    }

    @Test
    fun `should get a null script code for gibberish text of length one`() {
        assertNull(getByCode(randomStringOfLength(1)))
    }

    @Test
    fun `should get a null script code for gibberish text of length two`() {
        assertNull(getByCode(randomStringOfLength(2)))
    }

    @Test
    fun `should get a null script code for gibberish text of length three`() {
        assertNull(getByCode(randomStringOfLength(3)))
    }

    @Test
    fun `should get a null script code for gibberish text of length four`() {
        assertNull(getByCode(randomStringOfLength(4)))
    }

    @Test
    fun `should get a null script code for gibberish text of length five`() {
        assertNull(getByCode(randomStringOfLength(5)))
    }

    @Test
    fun `should get a null script code for a null code when casing is on explicitily`() {
        assertNull(getByCode(null, true))
    }

    @Test
    fun `should get a null script code for an empty string code when casing is on explicitily`() {
        assertNull(getByCode("", true))
    }

    @Test
    fun `should get a null script code for gibberish text of length one when casing is on explicitly`() {
        assertNull(getByCode(randomStringOfLength(1), true))
    }

    @Test
    fun `should get a null script code for gibberish text of length two when casing is on explicitly`() {
        assertNull(getByCode(randomStringOfLength(2), true))
    }

    @Test
    fun `should get a null script code for gibberish text of length three when casing is on explicitly`() {
        assertNull(getByCode(randomStringOfLength(3), true))
    }

    @Test
    fun `should get a null script code for gibberish text of length four when casing is on explicitly`() {
        assertNull(getByCode(randomStringOfLength(4), true))
    }

    @Test
    fun `should get a null script code for gibberish text of length five when casing is on explicitly`() {
        assertNull(getByCode(randomStringOfLength(5), true))
    }

    @Test
    fun `should get a null script code for a numeric code of zero`() {
        assertNull(getByCode(0))
    }

    @Test
    fun `should get a valid script code when given a valid numeric code`() {
        assertSame(ScriptCode.Jpan, getByCode(413))
    }

    @Test
    fun `should get the proper script code when case sensitivity is off explicitly`() {
        assertSame(ScriptCode.Jpan, getByCode("Jpan", false))
        assertSame(ScriptCode.Jpan, getByCode("JPan", false))
        assertSame(ScriptCode.Jpan, getByCode("JpAn", false))
        assertSame(ScriptCode.Jpan, getByCode("JpaN", false))
        assertSame(ScriptCode.Jpan, getByCode("jpan", false))
        assertSame(ScriptCode.Jpan, getByCode("jPan", false))
        assertSame(ScriptCode.Jpan, getByCode("jpAn", false))
        assertSame(ScriptCode.Jpan, getByCode("jpaN", false))
    }

    @Test
    fun `should get the proper script code when using the ignoreCase method`() {
        assertSame(ScriptCode.Jpan, getByCodeIgnoreCase("Jpan"))
        assertSame(ScriptCode.Jpan, getByCodeIgnoreCase("JPan"))
        assertSame(ScriptCode.Jpan, getByCodeIgnoreCase("JpAn"))
        assertSame(ScriptCode.Jpan, getByCodeIgnoreCase("JpaN"))
        assertSame(ScriptCode.Jpan, getByCodeIgnoreCase("jpan"))
        assertSame(ScriptCode.Jpan, getByCodeIgnoreCase("jPan"))
        assertSame(ScriptCode.Jpan, getByCodeIgnoreCase("jpAn"))
        assertSame(ScriptCode.Jpan, getByCodeIgnoreCase("jpaN"))
    }

    @Test
    fun `should get an Undefined script code when given an 'Undefined' string`() {
        assertSame(ScriptCode.Undefined, getByCode("Undefined"))
    }

    @Test
    fun `should get a null script code when given an 'UNDEFINED' string`() {
        assertNull(getByCode("UNDEFINED"))
    }

    @Test
    fun `should get the proper script code Undefined when using the ignoreCase method`() {
        assertSame(ScriptCode.Undefined, getByCodeIgnoreCase("UNDEFINED"))
    }

    @Test
    fun `should get a list of script codes using a regular expression`() {
        val list = ScriptCode.findByName("Egyptian.*")

        assertEquals(3, list.size.toLong())
        assertTrue(list.contains(ScriptCode.Egyd))
        assertTrue(list.contains(ScriptCode.Egyh))
        assertTrue(list.contains(ScriptCode.Egyp))
    }

    @Test
    fun `should get the ISO 15924 numeric value of the Script Code`() {
        assertEquals(413, ScriptCode.Jpan.numeric)
    }

    @Test
    fun `should throw an exception with a reasonable message when finding by name with a null search String`() {
        shouldThrow<IllegalArgumentException> {
            ScriptCode.findByName(regex = null)
        }.message should contain("regex is null")
    }

    @Test
    fun `should throw an exception with a reasonable message when finding by name with a null search Pattern`() {
        shouldThrow<IllegalArgumentException> {
            ScriptCode.findByName(pattern = null)
        }.message should contain("pattern is null")
    }

    private fun randomStringOfLength(stringLength: Int = 0): String {
        val charPool : List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9')
        val random = SecureRandom()
        val bytes = ByteArray(stringLength)
        random.nextBytes(bytes)

        return (0 until bytes.size)
                .map { i -> charPool.get(
                        (bytes[i] and
                                0xFF.toByte() and
                                (charPool.size-1).toByte()
                        ).toInt())
                }.joinToString("")
    }
}
