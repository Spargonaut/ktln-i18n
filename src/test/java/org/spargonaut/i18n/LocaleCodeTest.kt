/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package org.spargonaut.i18n


import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertSame
import org.junit.jupiter.api.Assertions.fail
import org.junit.jupiter.api.Disabled
import org.spargonaut.i18n.LocaleCode.Companion.getByCode
import org.spargonaut.i18n.LocaleCode.Companion.getByCountry
import org.spargonaut.i18n.LocaleCode.Companion.getByLanguage
import org.spargonaut.i18n.LocaleCode.Companion.getByLocale
import java.util.Locale


class LocaleCodeTest {
    private fun assertListEquals(list1: List<LocaleCode>?, list2: List<LocaleCode>?) {
        if (list1 == null) {
            if (list2 == null) {
                // Equals
                return
            } else {
                fail<Any>()
                return
            }
        } else if (list2 == null) {
            fail<Any>()
            return
        }

        val size = list1.size

        if (size != list2.size) {
            fail<Any>()
            return
        }

        for (i in 0 until size) {
            if (list1[i] !== list2[i]) {
                fail<Any>()
                return
            }
        }
    }

    @Test
    fun `should retreive all Locale codes associated with a language code`() {
        val expected = listOf(
                LocaleCode.ar,
                LocaleCode.ar_AE,
                LocaleCode.ar_BH,
                LocaleCode.ar_DZ,
                LocaleCode.ar_EG,
                LocaleCode.ar_IQ,
                LocaleCode.ar_JO,
                LocaleCode.ar_KW,
                LocaleCode.ar_LB,
                LocaleCode.ar_LY,
                LocaleCode.ar_MA,
                LocaleCode.ar_OM,
                LocaleCode.ar_QA,
                LocaleCode.ar_SA,
                LocaleCode.ar_SD,
                LocaleCode.ar_SY,
                LocaleCode.ar_TN,
                LocaleCode.ar_YE
        )

        val actual = getByLanguage(LanguageCode.ar)

        assertListEquals(expected, actual)
    }

    @Test
    fun `should produce an empty list when language code is null`() {
        val expected = listOf<LocaleCode>()
        val actual = getByLanguage(null as LanguageCode?)

        assertListEquals(expected, actual)
    }

    @Test
    fun `should produce all local codes for Switzerland`() {
        val expected = listOf(LocaleCode.de_CH, LocaleCode.fr_CH, LocaleCode.it_CH)
        val actual = getByCountry(CountryCode.CH)
        assertListEquals(expected, actual)
    }

    @Test
    fun `should produce an empty list when given a null CountryCode`() {
        val expected = listOf<LocaleCode>()
        val actual = getByCountry(null as CountryCode?)
        assertListEquals(expected, actual)
    }

    @Test
    fun `should produce the localeCode (languageCode) for German when using toLocal()`() {
        assertSame(Locale.GERMAN, LocaleCode.de.toLocale())
    }

    @Test
    fun `should produce the localeCode (languageCode) for English when using toLocal()`() {
        assertSame(Locale.ENGLISH, LocaleCode.en.toLocale())
    }

    @Test
    fun `should produce the localeCode (languageCode) for French when using toLocal()`() {
        assertSame(Locale.FRENCH, LocaleCode.fr.toLocale())
    }

    @Test
    fun `should produce the localeCode (languageCode) for Canadian French when using toLocal()`() {
        assertSame(Locale.CANADA_FRENCH, LocaleCode.fr_CA.toLocale())
    }

    @Test
    fun `should produce the localeCode (languageCode) for Italian when using toLocal()`() {
        assertSame(Locale.ITALIAN, LocaleCode.it.toLocale())
    }

    @Test
    fun `should produce the localeCode (languageCode) for Japanese when using toLocal()`() {
        assertSame(Locale.JAPANESE, LocaleCode.ja.toLocale())
    }

    @Test
    fun `should produce the localeCode (languageCode) for Korean when using toLocal()`() {
        assertSame(Locale.KOREAN, LocaleCode.ko.toLocale())
    }

    @Test
    fun `should produce the localeCode (languageCode) for Chinese when using toLocal()`() {
        assertSame(Locale.CHINESE, LocaleCode.zh.toLocale())
    }

    @Test
    fun `should produce the localeCode (languageCode) for Simplified Chinese when using toLocal()`() {
        assertSame(Locale.SIMPLIFIED_CHINESE, LocaleCode.zh_CN.toLocale())
    }

    @Test
    fun `should produce the localeCode (languageCode) for Traditional Chinese when using toLocal()`() {
        assertSame(Locale.TRADITIONAL_CHINESE, LocaleCode.zh_TW.toLocale())
    }

    @Test
    @Disabled("Can't really tell what this test does")
    fun test15() {
        val undefinedLocale = LocaleCode.undefined.toLocale()

        try {
            val root = Locale::class.java.getDeclaredField("ROOT").get(null) as Locale
            assertSame(root, undefinedLocale)
        } catch (e: Exception) {
            assertEquals("", undefinedLocale.language)
            assertEquals("", undefinedLocale.country)
        }

    }

    @Test
    fun `should produce the localeCode (languageCode) for Japanese when using getByLocal()`() {
        assertSame(LocaleCode.ja, getByLocale(Locale.JAPANESE))
    }

    @Test
    fun `should produce the localeCode (languageCode) for Japan when using getByLocal()`() {
        assertSame(LocaleCode.ja_JP, getByLocale(Locale.JAPAN))
    }

    @Test
    fun `should produce the Undefined localCode when using an empty string`() {
        assertSame(LocaleCode.undefined, getByLocale(Locale("", "")))
    }

    @Test
    fun `should produce the Undefined LocaleCode when given an all lowercase undefined language code string, and null country code`() {
        assertSame(LocaleCode.undefined, getByCode("undefined", null))
    }

    @Test
    fun `should produce null when given an all uppercase undefined language code string, and null country code`() {
        assertNull(LocaleCode.getByCode("UNDEFINED", null))
    }

    @Test
    fun `should produce the Undefined LocaleCode when given an all uppercase undefined language code string, and null country code and case insensitive`() {
        assertSame(LocaleCode.undefined, getByCode("UNDEFINED", null, false))
    }

    @Test
    fun `should produce the Undefined LocaleCode when given an all lowercase undefined language code string, and an all uppercase undefined country code`() {
        assertSame(LocaleCode.undefined, getByCode("undefined", "UNDEFINED"))
    }

    @Test
    fun `should produce null when given an all lowercase undefined language code string, and all lowercase undefined country code`() {
        assertNull(getByCode("undefined", "undefined"))
    }

    @Test
    fun `should produce the Undefined LocaleCode when given an all lowercase undefined language code string, and an all lowercase country code and case insensistive`() {
        assertSame(LocaleCode.undefined, getByCode("undefined", "undefined", false))
    }

    @Test
    fun `should produce the Undefined LocaleCode when given an all lowercase undefined language code string`() {
        assertSame(LocaleCode.undefined, getByCode("undefined"))
    }

    @Test
    fun `should produce null when given an all uppercase undefined language code string`() {
        assertNull(getByCode("UNDEFINED"))
    }

    @Test
    fun `should produce the Undefined LocaleCode when given an all uppercase undefined language code string, and case insensitive`() {
        assertSame(LocaleCode.undefined, getByCode("UNDEFINED", false))
    }

    @Test
    fun `should produce the Undefined LocaleCode when given a mixed case language code, country code string combo separated by a hyphen`() {
        assertSame(LocaleCode.undefined, getByCode("undefined-UNDEFINED"))
    }

    @Test
    fun `should produce null when given an all lowercase language code, country code string combo separated by a hyphen`() {
        assertNull(getByCode("undefined-undefined"))
    }

    @Test
    fun `should produce the Undefined LocaleCode when given an all lowercase language code, country code string combo separated by a hyphen and case insensitive`() {
        assertSame(LocaleCode.undefined, getByCode("undefined-undefined", false))
    }

    @Test
    fun `should produce the Undefined LocaleCode when given an mixed case language code, country code string combo separated by an underscore and case insensitive`() {
        assertSame(LocaleCode.undefined, getByCode("undefined_UNDEFINED"))
    }

    @Test
    fun `should produce null when given an all lowercase language code, country code string combo separated by an underscore`() {
        assertNull(getByCode("undefined_undefined"))
    }

    @Test
    fun `should produce null when given an all lowercase language code, country code string combo separated by an underscore and case insensitive`() {
        assertSame(LocaleCode.undefined, getByCode("undefined_undefined", false))
    }

    @Test
    fun `should produce the appropriate LocaleCode when given the lowercase string of the country code`() {
        assertSame(LocaleCode.ja, getByCode("ja"))
    }

    @Test
    fun `should produce null when given the uppercase string of the country code`() {
        assertNull(getByCode("JA"))
    }

    @Test
    fun `should produce the appropriate LocaleCode when given the uppercase string of the country code and case insensitive`() {
        assertSame(LocaleCode.ja, getByCode("JA", false))
    }

    @Test
    fun `should produce the appropriate LocaleCode when given the mixed case string of the language and country code separated by a hyphen`() {
        assertSame(LocaleCode.ja_JP, getByCode("ja-JP"))
    }

    @Test
    fun `should produce null when given an all lowercase string of the language and country code separated by a hyphen`() {
        assertNull(getByCode("ja-jp"))
    }

    @Test
    fun `should produce the appropriate LocaleCode when given an all lowercase string of the language and country code and case insensitive separated by a hyphen`() {
        assertSame(LocaleCode.ja_JP, getByCode("ja-jp", false))
    }

    @Test
    fun `should produce the appropriate LocaleCode when given the mixed case string of the language and country code separated by an underscore`() {
        assertSame(LocaleCode.ja_JP, getByCode("ja_JP"))
    }

    @Test
    fun `should produce null when given an all lowercase string of the language and country code separated by an underscore`() {
        assertNull(getByCode("ja_jp"))
    }

    @Test
    fun `should produce the appropriate LocaleCode when given an all lowercase string of the language and country code and case insensitive separated by an underscore`() {
        assertSame(LocaleCode.ja_JP, getByCode("ja_jp", false))
    }

    @Test
    fun `should produce null when given a mixed case string of the language and country code separated by a plus symbol`() {
        assertNull(getByCode("ja+JP"))
    }

    @Test
    fun `should produce null when given null`() {
        assertNull(getByCode(null))
    }

    @Test
    fun `should produce null with getByCode() with an empty string`() {
        assertNull(getByCode(""))
    }
}
