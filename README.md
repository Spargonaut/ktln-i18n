i18n
====
NOTE:  as of this writing, until this message is removed, this README is out of date and innacurate.

Overview
--------

Package to support internationalization, containing ISO 3166-1 country code enum,
ISO 639-1 language code enum, ISO 15924 script code enum, etc.

| Class                | Description                                                  |
|:---------------------|:-------------------------------------------------------------|
| `CountryCode`        | ISO 3166-1 country code.                                     |
| `LanguageCode`       | ISO 639-1 language code.                                     |
| `LanguageAlpha3Code` | ISO 639-2 language code.                                     |
| `LocaleCode`         | Available locales whose format match either 'xx' or 'xx-XX'. |
| `ScriptCode`         | ISO 15924 script code.                                       |
| `CurrencyCode`       | ISO 4217 currency code.                                      |


License
-------

  Apache License, Version 2.0


Gradle
------

```gradle
TODO - add gradle dependency example
```


Source Code
-----------

  <code>https://gitlab.com/Spargonaut/ktln-i18n</code>


JavaDoc
-------
TODO - add location of documentation
  <code></code>


Example
-------

```kotlin
// List all the country codes.
TODO - add example

// List all the language codes.
TODO - add example

// List all the locale codes.
TODO - add example

// List all the script codes.
TODO - add example

// List all the currency codes.
TODO - add example
```


See Also
--------

* [nv-i18n @ GitHub](https://github.com/TakahikoKawasaki/nv-i18n)
* Country Code [ISO 3166-1](http://en.wikipedia.org/wiki/ISO_3166-1)
* Country Code [ISO 3166-1 alpha-2](http://en.wikipedia.org/wiki/ISO_3166-1_alpha-2)
* Country Code [ISO 3166-1 alpha-3](http://en.wikipedia.org/wiki/ISO_3166-1_alpha-3)
* Country Code [ISO 3166-1 numeric](http://en.wikipedia.org/wiki/ISO_3166-1_numeric)
* Language Code [ISO 639-1](http://en.wikipedia.org/wiki/ISO_639-1)
* Language Alpha3 Code [ISO 639-2](http://en.wikipedia.org/wiki/ISO_639-2)
* Script Code [ISO 15924](http://en.wikipedia.org/wiki/ISO_15924)
* Currency Code [ISO 4217](http://en.wikipedia.org/wiki/ISO_4217)


TODO
----

* Remove Redundant code comments
* Set up build pipeline
* Remove ability to return null
* Improve test names and tests
* Add documentation location
* Add gradle import example
* Add Code Examples
* To add missing entries to CountryCode.
* To add international telephone dial number.

DEVELOPMENT
-----------

This repository is IDE independent.  Use any tools you prefer.

These commands are currently used for development:  
`./gradlew build` to build the application
  
`./gradlew test` to run tests and generate test coverage.  
Reports are located at ./build/reports/jacoco/test/html/index.html  


Note
----

This ktln-i18n extends the work at https://github.com/TakahikoKawasaki/nv-i18n using kotlin
